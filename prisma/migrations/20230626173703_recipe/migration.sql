-- CreateEnum
CREATE TYPE "RecipeCategorie" AS ENUM ('breakfast', 'lunch', 'dinner', 'snack', 'dessert');

-- CreateTable
CREATE TABLE "Recipe" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "desc" TEXT NOT NULL,
    "categorie" "RecipeCategorie" NOT NULL,

    CONSTRAINT "Recipe_pkey" PRIMARY KEY ("id")
);
