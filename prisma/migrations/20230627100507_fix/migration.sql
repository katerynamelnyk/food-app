/*
  Warnings:

  - The values [eggs] on the enum `IngredientName` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "IngredientName_new" AS ENUM ('cheese', 'egg', 'flour', 'baking_powder');
ALTER TABLE "Ingredient" ALTER COLUMN "name" TYPE "IngredientName_new" USING ("name"::text::"IngredientName_new");
ALTER TYPE "IngredientName" RENAME TO "IngredientName_old";
ALTER TYPE "IngredientName_new" RENAME TO "IngredientName";
DROP TYPE "IngredientName_old";
COMMIT;
