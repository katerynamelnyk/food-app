/*
  Warnings:

  - You are about to drop the column `categorie` on the `Recipe` table. All the data in the column will be lost.
  - Added the required column `category` to the `Recipe` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "RecipeCategory" AS ENUM ('breakfast', 'lunch', 'dinner', 'snack', 'dessert');

-- AlterTable
ALTER TABLE "Recipe" DROP COLUMN "categorie",
ADD COLUMN     "category" "RecipeCategory" NOT NULL;

-- DropEnum
DROP TYPE "RecipeCategorie";
