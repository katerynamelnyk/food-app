-- CreateEnum
CREATE TYPE "Meal" AS ENUM ('breakfast', 'lunch', 'dinner', 'snack');

-- CreateEnum
CREATE TYPE "FoodCategory" AS ENUM ('vegetables', 'fruits', 'cereals', 'makaroni', 'bakery', 'dairy', 'sweets', 'beverages', 'snacks', 'meat', 'seafood', 'desserts', 'eggs', 'nuts', 'other');

-- CreateTable
CREATE TABLE "DayDiary" (
    "id" TEXT NOT NULL,
    "userId" INTEGER,
    "date" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "DayDiary_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "MealItem" (
    "id" TEXT NOT NULL,
    "meal" "Meal" NOT NULL,
    "dayDiaryId" TEXT,

    CONSTRAINT "MealItem_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Food" (
    "id" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "category" "FoodCategory" NOT NULL,
    "mealItemId" TEXT,

    CONSTRAINT "Food_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "DayDiary" ADD CONSTRAINT "DayDiary_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MealItem" ADD CONSTRAINT "MealItem_dayDiaryId_fkey" FOREIGN KEY ("dayDiaryId") REFERENCES "DayDiary"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Food" ADD CONSTRAINT "Food_mealItemId_fkey" FOREIGN KEY ("mealItemId") REFERENCES "MealItem"("id") ON DELETE SET NULL ON UPDATE CASCADE;
