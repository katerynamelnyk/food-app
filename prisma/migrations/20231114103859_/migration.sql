/*
  Warnings:

  - Made the column `userId` on table `DayDiary` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "DayDiary" DROP CONSTRAINT "DayDiary_userId_fkey";

-- AlterTable
ALTER TABLE "DayDiary" ALTER COLUMN "userId" SET NOT NULL;

-- AddForeignKey
ALTER TABLE "DayDiary" ADD CONSTRAINT "DayDiary_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
