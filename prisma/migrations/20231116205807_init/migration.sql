/*
  Warnings:

  - Added the required column `glassOfWater` to the `DayDiary` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "DayDiary" ADD COLUMN     "glassOfWater" INTEGER NOT NULL;
