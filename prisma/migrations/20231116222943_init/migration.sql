-- DropForeignKey
ALTER TABLE "DayDiary" DROP CONSTRAINT "DayDiary_userId_fkey";

-- AlterTable
ALTER TABLE "DayDiary" ALTER COLUMN "userId" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "DayDiary" ADD CONSTRAINT "DayDiary_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE SET NULL ON UPDATE CASCADE;
