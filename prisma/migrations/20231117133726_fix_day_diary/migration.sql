/*
  Warnings:

  - Added the required column `steps` to the `DayDiary` table without a default value. This is not possible if the table is not empty.
  - Added the required column `workOut` to the `DayDiary` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "DayDiary" ADD COLUMN     "steps" INTEGER NOT NULL,
ADD COLUMN     "workOut" BOOLEAN NOT NULL;
