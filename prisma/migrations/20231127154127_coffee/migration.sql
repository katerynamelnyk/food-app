/*
  Warnings:

  - Added the required column `cupOfCoffee` to the `DayDiary` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "DayDiary" ADD COLUMN     "cupOfCoffee" INTEGER NOT NULL;
