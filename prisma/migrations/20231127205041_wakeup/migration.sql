/*
  Warnings:

  - Added the required column `wakeUpTime` to the `DayDiary` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "DayDiary" ADD COLUMN     "wakeUpTime" TIME(6) NOT NULL;
