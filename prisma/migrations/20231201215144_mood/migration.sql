-- CreateEnum
CREATE TYPE "MoodType" AS ENUM ('grateful', 'energetic', 'productive', 'peaceful', 'joyful', 'excellent', 'good', 'neutral', 'challenging', 'bad', 'terrible');

-- AlterTable
ALTER TABLE "DayDiary" ADD COLUMN     "mood" "MoodType" NOT NULL DEFAULT 'neutral';
