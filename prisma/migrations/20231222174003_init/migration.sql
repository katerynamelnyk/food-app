/*
  Warnings:

  - You are about to drop the column `category` on the `Recipe` table. All the data in the column will be lost.
  - You are about to drop the column `desc` on the `Recipe` table. All the data in the column will be lost.
  - Changed the type of `name` on the `Ingredient` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Changed the type of `measure` on the `Ingredient` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.
  - Added the required column `dairyFree` to the `Recipe` table without a default value. This is not possible if the table is not empty.
  - Added the required column `image` to the `Recipe` table without a default value. This is not possible if the table is not empty.
  - Added the required column `instructions` to the `Recipe` table without a default value. This is not possible if the table is not empty.
  - Added the required column `readyInMinutes` to the `Recipe` table without a default value. This is not possible if the table is not empty.
  - Added the required column `vegan` to the `Recipe` table without a default value. This is not possible if the table is not empty.
  - Added the required column `veryHealthy` to the `Recipe` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "Ingredient" DROP CONSTRAINT "Ingredient_recipeId_fkey";

-- AlterTable
ALTER TABLE "Ingredient" DROP COLUMN "name",
ADD COLUMN     "name" TEXT NOT NULL,
DROP COLUMN "measure",
ADD COLUMN     "measure" TEXT NOT NULL;

-- AlterTable
ALTER TABLE "Recipe" DROP COLUMN "category",
DROP COLUMN "desc",
ADD COLUMN     "dairyFree" BOOLEAN NOT NULL,
ADD COLUMN     "image" TEXT NOT NULL,
ADD COLUMN     "ingredients" INTEGER,
ADD COLUMN     "instructions" TEXT NOT NULL,
ADD COLUMN     "readyInMinutes" INTEGER NOT NULL,
ADD COLUMN     "vegan" BOOLEAN NOT NULL,
ADD COLUMN     "veryHealthy" BOOLEAN NOT NULL;

-- DropEnum
DROP TYPE "IngredientName";

-- DropEnum
DROP TYPE "Measure";

-- DropEnum
DROP TYPE "RecipeCategory";

-- AddForeignKey
ALTER TABLE "Recipe" ADD CONSTRAINT "Recipe_ingredients_fkey" FOREIGN KEY ("ingredients") REFERENCES "Ingredient"("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
