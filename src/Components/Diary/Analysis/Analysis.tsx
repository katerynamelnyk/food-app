import { useState } from "react";
import {
  CoffeeDrinkingAnalysis,
  StepsAnalysis,
  TimeAnalysis,
  WaterDrinkingAnalysis,
  WorkoutAnalysis,
  FoodAnalysis,
  MoodAnalysis,
  Button,
} from "@/components";
import styles from "./Analysis.module.scss";

type StatisticsButton = {
  title: string;
  component: JSX.Element;
};

export const Analysis = ({ data }: { data: DayDiary[] }) => {
  const BUTTONS_DATA: StatisticsButton[] = [
    {
      title: "meal",
      component: <FoodAnalysis dayDiaries={data} />,
    },
    {
      title: "water",
      component: <WaterDrinkingAnalysis dayDiaries={data} />,
    },
    {
      title: "coffee",
      component: <CoffeeDrinkingAnalysis dayDiaries={data} />,
    },
    {
      title: "steps",
      component: <StepsAnalysis dayDiaries={data} />,
    },
    {
      title: "workout",
      component: <WorkoutAnalysis dayDiaries={data} />,
    },
    {
      title: "time",
      component: <TimeAnalysis dayDiaries={data} />,
    },
    {
      title: "mood",
      component: <MoodAnalysis dayDiaries={data} />,
    },
  ];

  const [activeStatisticsButton, setActiveStatisticsButton] =
    useState<StatisticsButton>(BUTTONS_DATA[0]);

  const renderedButtons = BUTTONS_DATA.map((button, index) => (
    <Button
      key={index}
      type="small"
      title={button.title}
      className={
        button.title === activeStatisticsButton.title
          ? styles["active-btn"]
          : undefined
      }
      onClick={() => setActiveStatisticsButton(button)}
    />
  ));

  return (
    <div className={styles.main}>
      <div>{renderedButtons}</div>
      <div className={styles["component-container"]}>
        {activeStatisticsButton.component}
      </div>
    </div>
  );
};
