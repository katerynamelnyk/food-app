import Image from "next/image";
import { convertDateForUI } from "@/utility";
import styles from "./CoffeeDrinkingAnalysis.module.scss";

type Props = {
  dayDiaries: DayDiary[];
};

const NORMAL_AMOUNT_OF_CUPS_PER_DAY = 4;

export const CoffeeDrinkingAnalysis = ({ dayDiaries }: Props) => {
  const coffeeData = dayDiaries.map((day) => ({
    date: day.date,
    cupOfCoffee: day.cupOfCoffee,
  }));

  const averageAmountOfCups =
    coffeeData.reduce((accum, curVal) => accum + curVal.cupOfCoffee, 0) /
    coffeeData.length;

  const differenceBetweenNormal =
    100 -
    Math.round((averageAmountOfCups / NORMAL_AMOUNT_OF_CUPS_PER_DAY) * 100);

  const summaryBlock = (
    <div className={styles.summary}>
      <p>
        Average amount of drinked coffee is {averageAmountOfCups.toFixed(1)}{" "}
        cups per day.
      </p>
      <p>
        {averageAmountOfCups > NORMAL_AMOUNT_OF_CUPS_PER_DAY
          ? `You drink on ${differenceBetweenNormal}% more coffee then normal`
          : `It's less then normal, so it's ok :)`}
      </p>
    </div>
  );

  const renderedCupsOfCoffee = coffeeData.map((item, index) => {
    const cups =
      item.cupOfCoffee > 0 ? (
        new Array(item.cupOfCoffee)
          .fill(undefined)
          .map((_, cupIndex) => (
            <Image
              key={cupIndex}
              width={30}
              height={30}
              src="coffee-icon.svg"
              alt="cup of coffee"
            />
          ))
      ) : (
        <Image
          src="close-round-line-icon.svg"
          width={30}
          height={30}
          alt="no cup"
        />
      );

    return (
      <div key={index} className={styles["day-wrapper"]}>
        <p className={styles.date}>{convertDateForUI(item.date)}</p>
        <div>{cups}</div>
      </div>
    );
  });

  return (
    <div className={styles.main}>
      <h2 className={styles.header}>What about drinking coffee?</h2>
      <div className={styles["table-container"]}>{renderedCupsOfCoffee}</div>
      {summaryBlock}
    </div>
  );
};
