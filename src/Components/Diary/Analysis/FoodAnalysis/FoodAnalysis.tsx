import { ChangeEvent, useState } from "react";
import { FoodPieChart } from "@/components";
import { DropDown, Button } from "@/components/UI";
import { Meal } from "@prisma/client";
import { convertStringToMeal } from "@/utility";
import styles from "./FoodAnalysis.module.scss";

type Props = {
  dayDiaries: DayDiary[];
};

export const FoodAnalysis = ({ dayDiaries }: Props) => {
  const [selectedMeal, setSelectedMeal] = useState<Meal>("breakfast");
  const [isShowAnalysisForMeal, setIsShowAnalysisForSelectedMeal] =
    useState(false);

  const handleSelectMeal = (e: ChangeEvent<HTMLSelectElement>) => {
    const convertedValue = convertStringToMeal(e.currentTarget.value);
    setSelectedMeal(convertedValue);
  };

  const handleToggleAnalysis = () => {
    setIsShowAnalysisForSelectedMeal((prev) => !prev);
  };

  return (
    <div className={styles.main}>
      <div className={styles["chart-wrapper"]}>
        <FoodPieChart dayDiaries={dayDiaries} />
      </div>
      <div className={styles["selected-chart-wrapper"]}>
        <div className={styles["button-container"]}>
          <DropDown
            name="meal"
            title="Select Meal"
            value={selectedMeal}
            options={Object.values(Meal)}
            onChange={handleSelectMeal}
          />
          <Button
            type="small"
            onClick={handleToggleAnalysis}
            title={isShowAnalysisForMeal ? "Hide" : "Show"}
          />
        </div>
        {isShowAnalysisForMeal && (
          <FoodPieChart dayDiaries={dayDiaries} selectedMeal={selectedMeal} />
        )}
      </div>
    </div>
  );
};
