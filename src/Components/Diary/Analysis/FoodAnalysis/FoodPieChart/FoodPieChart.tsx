import { Chart } from "react-google-charts";
import { FoodCategory } from "@prisma/client";
import { FoodSummary } from "@/components";
import styles from "./FoodPieChart.module.scss";

type Props = {
  dayDiaries: DayDiary[];
  selectedMeal?: Meal;
};

type SortedFoodCategories = {
  [K in FoodCategory]: number;
};

export const FoodPieChart = ({ dayDiaries, selectedMeal }: Props) => {
  const sortedFoodCategories: SortedFoodCategories = {
    vegetables: 0,
    fruits: 0,
    cereals: 0,
    makaroni: 0,
    bakery: 0,
    dairy: 0,
    sweets: 0,
    beverages: 0,
    snacks: 0,
    meat: 0,
    seafood: 0,
    desserts: 0,
    eggs: 0,
    nuts: 0,
    other: 0,
  };

  dayDiaries.forEach((day) => {
    day?.mealItems.forEach((meal) => {
      !selectedMeal
        ? meal?.food.forEach(
            (item) => (sortedFoodCategories[item.category] += 1)
          )
        : meal.meal === selectedMeal &&
          meal?.food.forEach(
            (item) => (sortedFoodCategories[item.category] += 1)
          );
    });
  });
  const percentage =
    100 /
    Object.values(sortedFoodCategories).reduce(
      (accum, curVal) => curVal + accum,
      0
    );

  const categoriesWithPercentage = Object.entries(sortedFoodCategories)
    .map(([key, value]) => value !== 0 && [key, value * percentage])
    .filter((item) => item !== false);

  const categoriesWithPercentageForChart = [...categoriesWithPercentage];
  categoriesWithPercentageForChart.unshift(["Food categories", "% from all"]);
  const options = {
    title: "Food Categories",
    is3D: selectedMeal ? false : true,
  };

  return (
    <div className={styles.main}>
      <h1 className={styles.header}>
        What about food for {!selectedMeal ? "all meals" : selectedMeal}?
      </h1>
      {categoriesWithPercentage.length > 1 ? (
        <div className={styles.content}>
          <Chart
            chartType="PieChart"
            data={categoriesWithPercentageForChart}
            options={options}
            width={"100%"}
            height={"350px"}
          />
          <FoodSummary
            categories={categoriesWithPercentage}
            dayDiaries={dayDiaries}
            selectedMeal={selectedMeal}
          />
        </div>
      ) : (
        <p>You have not added any food for this meal category.</p>
      )}
    </div>
  );
};
