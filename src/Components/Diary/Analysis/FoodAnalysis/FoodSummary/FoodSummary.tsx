import { convertTimeForUI, getAverageTime } from "@/utility";
import styles from "./FoodSummary.module.scss";

type Props = {
  categories: (false | (string | number)[])[];
  dayDiaries?: DayDiary[];
  selectedMeal?: Meal;
};

type MealProps = {
  dayDiaries: DayDiary[];
  selectedMeal: Meal;
};

const SelectedMealAnalysis = ({ selectedMeal, dayDiaries }: MealProps) => {
  const filteredDates: string[] = [];
  dayDiaries.forEach((day) =>
    day.mealItems.forEach(
      (mealItem) =>
        mealItem.meal === selectedMeal &&
        filteredDates.push(convertTimeForUI(mealItem.time))
    )
  );
  const averageTime = getAverageTime(filteredDates);

  return (
    <div>
      <p>
        Average {selectedMeal} time is {averageTime}.
      </p>
    </div>
  );
};

export const FoodSummary = ({
  categories,
  dayDiaries,
  selectedMeal,
}: Props) => {
  const categoriesText = (
    <div className={styles.text}>
      <p>Your food consists of {categories.length} different categories.</p>
    </div>
  );
  return (
    <div className={styles.main}>
      {selectedMeal && dayDiaries && (
        <SelectedMealAnalysis
          selectedMeal={selectedMeal}
          dayDiaries={dayDiaries}
        />
      )}
      {categoriesText}
    </div>
  );
};
