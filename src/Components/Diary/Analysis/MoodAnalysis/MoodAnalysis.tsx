import { convertDateForUI } from "@/utility";
import styles from "./MoodAnalysis.module.scss";

type Props = {
  dayDiaries: DayDiary[];
};

const MOODS = [
  {
    name: "excellent",
    range: 11,
    level: "high",
  },
  {
    name: "energetic",
    range: 10,
    level: "high",
  },
  {
    name: "productive",
    range: 9,
    level: "high",
  },
  {
    name: "peaceful",
    range: 8,
    level: "high",
  },
  {
    name: "joyful",
    range: 7,
    level: "high",
  },
  {
    name: "grateful",
    range: 6,
    level: "high",
  },
  {
    name: "good",
    range: 5,
    level: "high",
  },
  {
    name: "neutral",
    range: 4,
    level: "normal",
  },
  {
    name: "challenging",
    range: 3,
    level: "low",
  },
  {
    name: "bad",
    range: 2,
    level: "low",
  },
  {
    name: "terrible",
    range: 1,
    level: "low",
  },
];

const DEFAULT_MOOD = {
  name: "none",
  range: 0,
  level: "none",
};

const getMoodInfo = (moodName: string) => {
  return MOODS.find((item) => item.name === moodName) || DEFAULT_MOOD;
};

export const MoodAnalysis = ({ dayDiaries }: Props) => {
  const moodData = dayDiaries.map((day) => ({
    date: day.date,
    mood: day.mood,
  }));

  const renderedMoodAnalysis = moodData.map((item, index) => {
    const { level } = getMoodInfo(item.mood);

    return (
      <div key={index} className={styles["day-container"]}>
        <p className={styles.date}>{convertDateForUI(item.date)}</p>
        <p className={styles[level]}>{item.mood}</p>
      </div>
    );
  });

  return (
    <div className={styles.main}>
      <h2 className={styles.header}>What about mood?</h2>
      <div className={styles.container}>{renderedMoodAnalysis}</div>
    </div>
  );
};
