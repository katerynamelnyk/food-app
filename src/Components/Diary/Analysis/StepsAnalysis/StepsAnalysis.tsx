import { convertDateForUI } from "@/utility";
import styles from "./StepsAnalysis.module.scss";

type Props = {
  dayDiaries: DayDiary[];
};

const NORMAL_AMOUNT_OF_STEPS_PER_DAY = 8000;

//? separate if statements

export const StepsAnalysis = ({ dayDiaries }: Props) => {
  //! array with the analize data
  const analyzeData: string[] = [];

  const stepsData = dayDiaries.map((day) => ({
    steps: day.steps,
    date: day.date,
  }));

  const steps = stepsData.map((item) => item.steps);

  //! early exit if there is no info provided
  if (steps.every((daySteps) => daySteps === 0)) {
    analyzeData.push(
      "Unfortunatelly, you have not added info about steps you made these days."
    );

    return (
      <ul>
        {analyzeData.map((item, index) => (
          <ul key={index} className={styles["steps-item"]}>
            {item}
          </ul>
        ))}
      </ul>
    );
  }

  //! filtered array only with NO_ZERO days
  const daysWithSteps = steps.filter((daySteps) => +daySteps > 0);

  //! average steps
  const averageSteps =
    daysWithSteps.reduce((accum, curVal) => accum + curVal, 0) /
    daysWithSteps.length;
  analyzeData.push(
    `The average amount of steps is ${Math.round(averageSteps)}.`
  );

  //! comperison with normal amount
  const differenceBetweenNormal =
    100 - Math.round((averageSteps / NORMAL_AMOUNT_OF_STEPS_PER_DAY) * 100);
  analyzeData.push(
    differenceBetweenNormal > NORMAL_AMOUNT_OF_STEPS_PER_DAY
      ? `It's on ${differenceBetweenNormal}% higher then normal. Great!`
      : `Unfortunatelly it's on ${differenceBetweenNormal}% lower then normal. Keep pushing!`
  );

  //! highest amount of steps
  const highestAmountOfSteps = daysWithSteps.reduce(
    (curVal, accum) => Math.max(curVal, accum),
    daysWithSteps[0]
  );
  analyzeData.push(
    `The highest is ${highestAmountOfSteps}. ${
      highestAmountOfSteps > NORMAL_AMOUNT_OF_STEPS_PER_DAY
        ? "Great job!"
        : "Still less then recommended amount."
    }`
  );

  //! lowest amount of steps
  const lowestAmountOfSteps = daysWithSteps.reduce(
    (curVal, accum) => Math.min(curVal, accum),
    daysWithSteps[0]
  );
  analyzeData.push(`The lowest is ${lowestAmountOfSteps}.`);

  //! steps diagram

  const roundedHighestAmountOfSteps =
    Math.ceil((highestAmountOfSteps + highestAmountOfSteps * 0.1) / 1000) *
    1000;

  const stepsDiagram = stepsData.map((day, index) => (
    <div key={index} className={styles["steps-diagram-container"]}>
      <p className={styles["highest-amount"]}>{roundedHighestAmountOfSteps}</p>
      <div className={styles["steps-diagram-outter"]}>
        <div
          className={styles["steps-diagram-inner"]}
          style={{
            height: `${(day.steps / roundedHighestAmountOfSteps) * 100}%`,
          }}
        >
          <p className={styles["inner-steps"]}>{day.steps}</p>
        </div>
      </div>
      <div className={styles["steps-diagram-date"]}>
        {convertDateForUI(day.date)}
      </div>
    </div>
  ));

  return (
    <div className={styles.main}>
      <h2 className={styles.header}>What about steps?</h2>
      <div className={styles["diagram-wrapper"]}>{stepsDiagram}</div>
      <ul className={styles["summary-wrapper"]}>
        {analyzeData.map((item, index) => (
          <ul key={index} className={styles["steps-item"]}>
            {item}
          </ul>
        ))}
      </ul>
    </div>
  );
};
