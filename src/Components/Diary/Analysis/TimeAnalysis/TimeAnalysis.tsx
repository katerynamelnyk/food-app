import Image from "next/image";
import { convertDateForUI, convertTimeForUI, getAverageTime } from "@/utility";
import styles from "./TimeAnalysis.module.scss";

type Props = {
  dayDiaries: DayDiary[];
};

export const TimeAnalysis = ({ dayDiaries }: Props) => {
  const timeData = dayDiaries.map((day) => ({
    date: day.date,
    wakeUp: day.wakeUpTime,
    goSleep: day.goSleepTime,
  }));

  const wakeUpTime = timeData.map((item) => convertTimeForUI(item.wakeUp));
  const averageWakeUpTime = getAverageTime(wakeUpTime);

  const goSleepTime = timeData.map((item) =>
    item.goSleep !== null ? convertTimeForUI(item.goSleep) : null
  );

  const filteredGoSleepTime = goSleepTime.filter(
    (time) => typeof time === "string"
  );

  const averageGoSleepTime =
    filteredGoSleepTime.length > 0 && getAverageTime(filteredGoSleepTime);

  const summary = (
    <div className={styles.summary}>
      <p>Average wake up time is {averageWakeUpTime}.</p>
      {averageGoSleepTime && (
        <p>Average go to sleep time is {averageGoSleepTime}.</p>
      )}
    </div>
  );

  const renderedDates = (
    <div className={styles.column}>
      <Image
        src="/schedule-calendar-icon.svg"
        alt="wake up"
        width={30}
        height={30}
        className={styles.date}
      />
      {timeData.map((item) => (
        <p key={item.date} className={styles.date}>
          {convertDateForUI(item.date)}
        </p>
      ))}
    </div>
  );
  const renderedWakeUp = (
    <div className={styles.column}>
      <Image
        src="/wake-up-icon.svg"
        alt="wake up"
        width={30}
        height={30}
        className={styles["wake-up"]}
      />
      {timeData.map((item) => (
        <p key={item.date}>{convertTimeForUI(item.wakeUp)}</p>
      ))}
    </div>
  );
  const renderedGoSleep = (
    <div className={styles.column}>
      <Image
        src="/snooze-zzz-icon.svg"
        alt="wake up"
        width={30}
        height={30}
        className={styles["go-sleep"]}
      />
      {timeData.map((item) => (
        <p key={item.date}>
          {item.goSleep ? convertTimeForUI(item.goSleep) : "-"}
        </p>
      ))}
    </div>
  );

  return (
    <div className={styles.main}>
      <h2 className={styles.header}>What about time?</h2>
      <div className={styles.container}>
        {renderedDates}
        {renderedWakeUp}
        {renderedGoSleep}
      </div>
      {summary}
    </div>
  );
};
