import Image from "next/image";
import { convertDateForUI } from "@/utility";
import styles from "./WaterDrinkingAnalysis.module.scss";

type Props = {
  dayDiaries: DayDiary[];
};

const NORMAL_AMOUNT_OF_GLASSES_PER_DAY = 3;

export const WaterDrinkingAnalysis = ({ dayDiaries }: Props) => {
  const waterData = dayDiaries.map((day) => ({
    numOfGlasses: day.glassOfWater,
    date: day.date,
  }));

  const averageAmountOfGlasses =
    waterData.reduce((accum, curVal) => curVal.numOfGlasses + accum, 0) /
    waterData.length;

  const differenceBetweenNormal =
    100 -
    Math.round(
      (averageAmountOfGlasses / NORMAL_AMOUNT_OF_GLASSES_PER_DAY) * 100
    );

  const summaryBlock = (
    <div className={styles.summary}>
      <p>
        Average amount of drinked water is {averageAmountOfGlasses.toFixed(1)}{" "}
        glasses per day.
      </p>
      <p>
        {averageAmountOfGlasses > NORMAL_AMOUNT_OF_GLASSES_PER_DAY
          ? `You drink on ${differenceBetweenNormal}% more water then normal. It's good!`
          : `Unfortunatelly you drink on ${differenceBetweenNormal}% less water then normal. Try to increase it!`}
      </p>
    </div>
  );

  const renderedWaterDrinkingAnalysis = waterData.map((item, index) => {
    const renderedGlasses =
      item.numOfGlasses === 0 ? (
        <Image
          src="empty-glass-icon.svg"
          alt="empty glass of water"
          height={30}
          width={30}
        />
      ) : (
        Array(item.numOfGlasses)
          .fill(undefined)
          .map((_, glassIndex) => (
            <Image
              key={glassIndex}
              src="water-glass-color-icon.svg"
              alt="glass of water"
              height={30}
              width={30}
            />
          ))
      );

    return (
      <div key={index} className={styles["day-wrapper"]}>
        <p className={styles.date}>{convertDateForUI(item.date)}</p>
        <div>{renderedGlasses}</div>
      </div>
    );
  });

  return (
    <div className={styles.main}>
      <h2 className={styles.header}>What about drinking water?</h2>
      <div className={styles["table-container"]}>
        {renderedWaterDrinkingAnalysis}
      </div>
      {summaryBlock}
    </div>
  );
};
