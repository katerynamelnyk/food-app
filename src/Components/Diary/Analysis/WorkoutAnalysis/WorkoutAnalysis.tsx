import Image from "next/image";
import { convertDateForUI } from "@/utility";
import styles from "./WorkoutAnalysis.module.scss";

type Props = {
  dayDiaries: DayDiary[];
};

export const WorkoutAnalysis = ({ dayDiaries }: Props) => {
  const filteredData = dayDiaries.map((day) => ({
    isWorkOut: day.workOut,
    date: day.date,
  }));

  const renderedWorkoutAnalysis = filteredData.map((item, index) => (
    <div key={index} className={styles["workout-item"]}>
      <p className={styles.image}>
        {item.isWorkOut ? (
          <Image
            alt="workout"
            src="girl-exercise-pose-icon.svg"
            width={45}
            height={45}
          />
        ) : (
          <Image
            alt="no workout"
            src="close-round-line-icon.svg"
            width={30}
            height={45}
          />
        )}
      </p>
      <p className={styles.date}>{convertDateForUI(item.date)}</p>
    </div>
  ));

  return (
    <div className={styles.main}>
      <h2 className={styles.header}>What about workouts?</h2>
      <div className={styles["workouts-wrapper"]}>
        {renderedWorkoutAnalysis}
      </div>
    </div>
  );
};
