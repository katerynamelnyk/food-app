import { useRouter } from "next/navigation";
import Image from "next/image";
import { useDispatch } from "react-redux";
import { diaryActions } from "@/app/GlobalRedux/store";
import { convertTimeForUI } from "@/utility";
import { Button, DateItem, InfoItem, MealItem } from "@/components";
import styles from "./DayDiary.module.scss";

type Props = {
  data: DayDiary;
  isShowButtons?: boolean;
  isShowOnlyMeals?: boolean;
  onDelete?: (id?: string) => void;
  onClickDate?: () => void;
};

export const DayDiary = ({
  data,
  onDelete,
  isShowOnlyMeals,
  isShowButtons,
  onClickDate,
}: Props) => {
  const router = useRouter();

  const dispatch = useDispatch();

  const renderedMealItems = data?.mealItems.map((item) => (
    <MealItem key={item.id} {...item} isShortView={isShowOnlyMeals} />
  ));

  const handleEditDayDiary = () => {
    dispatch(diaryActions.setDiaryToEdit(data));
    router.push(`/diary/${data.id}`);
  };

  const handleClickOnDate = () => {
    onClickDate && onClickDate();
    dispatch(diaryActions.setSelectedDate(data.date));
  };

  return (
    <section
      className={isShowOnlyMeals ? `${styles["short-main"]}` : `${styles.main}`}
    >
      <Image
        src="/pin-fill.svg"
        alt="pin"
        width={35}
        height={35}
        className={styles["img-pin"]}
      />
      <div className={styles["data-container"]}>
        <div className={styles.date}>
          <DateItem
            date={data.date}
            onClickDate={onClickDate && handleClickOnDate}
          />
        </div>
        <div className={styles["diary-container"]}>
          <div className={styles["meals-container"]}>{renderedMealItems}</div>
          {!isShowOnlyMeals && (
            <div className={styles["infos-container"]}>
              <InfoItem
                title="Wake up Time"
                value={convertTimeForUI(data.wakeUpTime)}
              />
              {data.goSleepTime && (
                <InfoItem
                  title="Go To Sleep Time"
                  value={convertTimeForUI(data.goSleepTime)}
                />
              )}
              <InfoItem title="Glasses of Water" value={data.glassOfWater} />
              <InfoItem title="Cups of Coffee" value={data.cupOfCoffee} />
              <InfoItem title="Steps" value={data.steps} />
              <InfoItem title="Workout" value={data.workOut ? "+" : "-"} />
              <InfoItem title="Mood Of The Day" value={data.mood} />
            </div>
          )}
        </div>
      </div>
      {isShowButtons && (
        <div>
          <Button
            type="small"
            title="Delete"
            onClick={() => onDelete && onDelete(data.id)}
            className={styles["delete-btn"]}
          />
          <Button type="small" title="Edit" onClick={handleEditDayDiary} />
        </div>
      )}
    </section>
  );
};
