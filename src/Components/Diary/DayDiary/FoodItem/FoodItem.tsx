import styles from "./FoodItem.module.scss";

type Props = {
  food: Food;
  isShortView?: boolean;
};

export const FoodItem = ({ food, isShortView }: Props) => {
  let categoryClass = "";
  //add styles for all classes in css
  switch (food.category) {
    case "vegetables":
    case "sweets":
    case "bakery":
    case "dairy":
    case "fruits":
    case "cereals":
    case "beverages":
    case "snacks":
    case "other":
    case "meat":
    case "seafood":
    case "eggs":
    case "makaroni":
    case "desserts":
    case "nuts":
      categoryClass = food.category;
      break;
    default:
      categoryClass = "unknownCategory";
      break;
  }

  return (
    <div
      className={
        isShortView
          ? `${styles.container} ${styles["container-short"]}`
          : `${styles.container}`
      }
    >
      <div className={styles.description}>{food.description}</div>
      <div
        className={
          categoryClass && `${styles.category} ${styles[categoryClass]}`
        }
      >
        {food.category}
      </div>
    </div>
  );
};
