import styles from "./InfoItem.module.scss";

type Props = {
  title: string;
  value: string | number;
};

export const InfoItem = ({ title, value }: Props) => {
  return (
    <p className={styles["info-item"]}>
      <span className={styles.label}>{title}:</span>
      <span>{value}</span>
    </p>
  );
};
