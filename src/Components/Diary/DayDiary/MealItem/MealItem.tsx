import { convertTimeForUI } from "@/utility";
import { FoodItem } from "@/components";
import styles from "./MealItem.module.scss";
import Image from "next/image";

type MealItem = {
  meal: Meal;
  food: Food[];
  time: string;
  isShortView?: boolean;
};

export const MealItem = ({ meal, food, time, isShortView }: MealItem) => {
  //will render one specific item of day diary
  const renderedFood = food.map((item) => (
    <FoodItem key={item.id} food={item} isShortView={isShortView} />
  ));

  return (
    <div
      className={
        isShortView ? `${styles["container-short"]}` : `${styles.container}`
      }
    >
      <div className={styles.time}>
        <Image src="/clock.svg" width={20} height={20} alt="clock"/>
      <p >{convertTimeForUI(time)}</p>
      </div>
      {/* <p className={styles.time}>{convertTimeForUI(time)}</p> */}
      {isShortView && <p className={styles.meal}>{meal}</p>}
      <div className={styles.food}>{renderedFood}</div>
      {!isShortView && <p className={styles.meal}>{meal}</p>}
    </div>
  );
};
