import Calendar from "react-calendar";
import { useDispatch, useSelector } from "react-redux";
import { DiaryState, diaryActions } from "@/app/GlobalRedux/store";
import { convertDateToISOString } from "@/utility";
import "react-calendar/dist/Calendar.css";
import styles from "./DiaryCalendar.module.scss";

type ValuePiece = Date | null;
type Value = ValuePiece | ValuePiece[];

type Props = {
  isDateRange: boolean;
  dates: Date[];
  handleDateRange: (dates: string[]) => void;
};

export const DiaryCalendar = ({
  isDateRange,
  dates,
  handleDateRange,
}: Props) => {
  const dispatch = useDispatch();
  const selectedDate = useSelector(
    (state: DiaryState) => state.diary.selectedDate
  );

  const tileClassName = ({ date }: { date: Date }) => {
    const isInRange = dates.some(
      (userDate) =>
        convertDateToISOString(date) === convertDateToISOString(userDate)
    );

    if (convertDateToISOString(date) === convertDateToISOString(new Date())) {
      return styles.today;
    }

    if (convertDateToISOString(date) === convertDateToISOString(selectedDate)) {
      return styles.selected;
    }

    return isInRange ? styles.highlight : "";
  };

  const handleChangeCalendarData = (calendarData: Value) => {
    if (!calendarData) {
      throw new Error("Failed to get data from calendar!");
    }

    if (Array.isArray(calendarData)) {
      const convertedDates = calendarData.map((date) =>
        date !== null ? convertDateToISOString(date) : ""
      );
      handleDateRange(convertedDates);
      return;
    }

    const convertedDate = convertDateToISOString(calendarData);
    dispatch(diaryActions.setSelectedDate(convertedDate));
  };

  return (
    <Calendar
      value={new Date(selectedDate)}
      onChange={(calendarData: Value) => handleChangeCalendarData(calendarData)}
      className={styles["react-calendar"]}
      tileClassName={tileClassName}
      selectRange={isDateRange}
    />
  );
};
