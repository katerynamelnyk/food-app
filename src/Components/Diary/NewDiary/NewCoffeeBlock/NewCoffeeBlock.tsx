import Image from "next/image";
import { Button } from "@/components/UI";
import styles from "./NewCoffeeBlock.module.scss";

type Props = {
  cupOfCoffee: number | undefined;
  handleAddCoffee: () => void;
};

const NewCoffeeBlock = ({ cupOfCoffee, handleAddCoffee }: Props) => {
  if (cupOfCoffee === undefined) {
    return;
  }

  const renderedCupsOfCoffee = new Array(cupOfCoffee)
    .fill(undefined)
    .map((_, index) => {
      return (
        <Image
          key={index}
          src="/coffee-icon.svg"
          width={30}
          height={30}
          alt="cup of coffee"
        />
      );
    });

  return (
    <div className={styles.main}>
      <Button
        type="small"
        title="Add Cup Of Coffee"
        onClick={handleAddCoffee}
      />
      <div>
        {cupOfCoffee > 0 ? (
          renderedCupsOfCoffee
        ) : (
          <Image
            src="/coffee-bean-icon.svg"
            width={30}
            height={30}
            alt="coffee beans"
          />
        )}
      </div>
    </div>
  );
};

export default NewCoffeeBlock;
