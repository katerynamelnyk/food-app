"use client";

import { ChangeEvent, useState } from "react";
import { FoodCategory } from "@prisma/client";
import { convertStringToFoodCategory } from "@/utility";
import { Button, DropDown, Input } from "@/components/UI";
import styles from "./NewFoodBlock.module.scss";

type Props = {
  onAddFoodBlock: () => void;
  onSaveFoodData: (data: Food, isEdit?: boolean) => void;
  editedData?: Food;
};

const NewFoodBlock = ({
  onAddFoodBlock,
  onSaveFoodData,
  editedData,
}: Props) => {
  const [description, setDescription] = useState(
    editedData ? editedData.description : ""
  );
  const [category, setCategory] = useState<FoodCategory>(
    editedData ? editedData.category : "other"
  );
  const [isSaved, setIsSaved] = useState(false);

  const handleAddDescription = (e: ChangeEvent<HTMLInputElement>) => {
    setDescription(e.currentTarget.value);
  };
  const handleAddCategory = (e: ChangeEvent<HTMLSelectElement>) => {
    const convertedValue = convertStringToFoodCategory(e.currentTarget.value);
    setCategory(convertedValue);
  };

  const onSave = () => {
    if (description.trim().length === 0 || !category) {
      return;
    }
    const data: Food = {
      id: editedData ? editedData.id : Math.random().toString(),
      category,
      description,
    };
    onSaveFoodData(data, editedData && true);
    setIsSaved(true);
  };

  const saveBtnTitle = isSaved ? "Saved" : "Save";

  return (
    <div className={styles["food-container"]}>
      <Input
        type="text"
        labelTitle="Add Food Description"
        defaultValue={description}
        name="food"
        onChange={handleAddDescription}
      />
      <DropDown
        name="food category"
        title="Select Food category"
        options={Object.values(FoodCategory)}
        onChange={handleAddCategory}
        value={category}
      />
      <div className={styles["btn-container"]}>
        <Button
          type="small"
          className={styles.btn}
          title={saveBtnTitle}
          onClick={onSave}
          disabled={isSaved}
        />
        <Button
          type="small"
          className={styles.btn}
          title="Add Another"
          onClick={onAddFoodBlock}
        />
      </div>
    </div>
  );
};

export default NewFoodBlock;
