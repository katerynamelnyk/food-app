"use client";

import { ChangeEvent, useState } from "react";
import { Meal } from "@prisma/client";
import NewFoodBlock from "../NewFoodBlock/NewFoodBlock";
import NewTimeBlock from "../NewTimeBlock/NewTimeBlock";
import {
  convertHour,
  convertMinute,
  convertStringToMeal,
  convertTimeForDB,
} from "@/utility";
import { Button, DropDown } from "@/components/UI";
import styles from "./NewMealBlock.module.scss";

type Props = {
  onSaveMeal: (data: MealItem, foodData?: Food[]) => void;
  editedData?: MealItem;
};

const NewMealBlock = ({ onSaveMeal, editedData }: Props) => {
  const [foodBlocks, setFoodBlocks] = useState<JSX.Element[]>([]);
  const [foodData, setFoodData] = useState<Food[]>(
    editedData ? editedData.food : []
  );
  const [addedFoodData, setAddedFoodData] = useState<Food[]>([]);

  const [meal, setMeal] = useState<Meal>(
    editedData ? editedData.meal : "breakfast"
  );

  const [hour, setHour] = useState(
    editedData?.time ? convertHour(editedData.time) : ""
  );
  const [minute, setMinute] = useState(
    editedData?.time ? convertMinute(editedData.time) : ""
  );

  const handleAddHour = (e: ChangeEvent<HTMLInputElement>) => {
    setHour(e.currentTarget.value);
  };

  const handleAddMinute = (e: ChangeEvent<HTMLInputElement>) => {
    setMinute(e.currentTarget.value);
  };

  const [isSaved, setIsSaved] = useState(false);

  const handleSelectMeal = (e: ChangeEvent<HTMLSelectElement>) => {
    const convertedValue = convertStringToMeal(e.currentTarget.value);
    setMeal(convertedValue);
  };

  const handleAddFoodBlock = () => {
    setFoodBlocks((prevBlocks) => [
      ...prevBlocks,
      <NewFoodBlock
        key={prevBlocks.length}
        onAddFoodBlock={handleAddFoodBlock}
        onSaveFoodData={handleSaveFoodData}
      />,
    ]);
  };

  const handleSaveFoodData = (data: Food, isEdit?: boolean) => {
    if (!isEdit) {
      editedData
        ? setAddedFoodData((prevData) => [
            ...prevData,
            { ...data, mealItemId: editedData.id },
          ])
        : setFoodData((prevData) => [...prevData, data]);
    }

    if (isEdit) {
      setFoodData((prevData) =>
        prevData.map((item) => (item.id === data.id ? data : item))
      );
    }
  };

  const handleSaveMeal = () => {
    if (foodData.length === 0 || meal.trim().length === 0) {
      return;
    }
    const convertedTime = convertTimeForDB(hour, minute);
    const data: MealItem = {
      id: editedData ? editedData.id : Math.random().toString(),
      meal,
      food: foodData,
      time: convertedTime,
    };

    addedFoodData ? onSaveMeal(data, addedFoodData) : onSaveMeal(data);
    setIsSaved(true);
  };

  const saveBtnTitle = isSaved ? "Meal Saved" : "Save Meal";

  return (
    <div className={styles["meal-container"]}>
      <DropDown
        name="meal"
        title="Select Meal"
        value={meal}
        options={Object.values(Meal)}
        onChange={handleSelectMeal}
      />
      <NewTimeBlock
        title="Meal Time"
        handleHour={handleAddHour}
        handleMinute={handleAddMinute}
        editHour={hour}
        editMinute={minute}
        imageAlt="time"
        imageSrc="/alarm-line-icon.svg"
      />
      {!editedData && foodBlocks.length === 0 && (
        <Button type="small" onClick={handleAddFoodBlock} title="Add food" />
      )}
      {editedData &&
        editedData.food.map((foodItem) => (
          <NewFoodBlock
            key={foodItem.id}
            onAddFoodBlock={handleAddFoodBlock}
            onSaveFoodData={handleSaveFoodData}
            editedData={foodItem}
          />
        ))}
      {foodBlocks}
      <Button
        type="medium"
        title={saveBtnTitle}
        onClick={handleSaveMeal}
        className={styles["btn-save"]}
        disabled={isSaved}
      />
    </div>
  );
};

export default NewMealBlock;
