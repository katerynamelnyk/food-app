import { ChangeEvent } from "react";
import { MoodType } from "@prisma/client";
import { DropDown } from "@/components/UI";
import styles from "./NewMoodBlock.module.scss";

type Props = {
  mood: MoodType;
  handleMood: (e: ChangeEvent<HTMLSelectElement>) => void;
};

const NewMoodBlock = ({ mood, handleMood }: Props) => {
  return (
    <DropDown
      name="mood"
      title="Mood Of The Day"
      options={Object.values(MoodType)}
      value={mood}
      onChange={handleMood}
    />
  );
};

export default NewMoodBlock;
