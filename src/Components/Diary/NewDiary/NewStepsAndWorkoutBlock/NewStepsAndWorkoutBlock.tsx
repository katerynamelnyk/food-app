import { ChangeEvent } from "react";
import { Input } from "@/components/UI";
import styles from "./NewStepsAndWorkoutBlock.module.scss";

type Props = {
  handleSteps: (e: ChangeEvent<HTMLInputElement>) => void;
  handleWorkout: (e: ChangeEvent<HTMLInputElement>) => void;
  editedSteps?: number;
  editedIsWorkout?: boolean;
};

const NewStepsAndWorkoutBlock = ({
  handleSteps,
  handleWorkout,
  editedSteps,
  editedIsWorkout,
}: Props) => {
  return (
    <div className={styles.main}>
      <Input
        type="number"
        labelTitle="Add Steps"
        name="steps"
        onChange={handleSteps}
        defaultValue={editedSteps}
      />
      <Input
        type="checkbox"
        labelTitle="Have you workout?"
        name="workOut"
        onChange={handleWorkout}
        isChecked={editedIsWorkout}
      />
    </div>
  );
};

export default NewStepsAndWorkoutBlock;
