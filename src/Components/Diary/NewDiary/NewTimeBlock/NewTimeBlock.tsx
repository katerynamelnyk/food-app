import { ChangeEvent } from "react";
import Image from "next/image";
import { Input } from "@/components/UI";
import styles from "./NewTimeBlock.module.scss";

type Props = {
  title: string;
  imageSrc: string;
  imageAlt: string;
  handleHour: (e: ChangeEvent<HTMLInputElement>) => void;
  handleMinute: (e: ChangeEvent<HTMLInputElement>) => void;
  editHour?: string;
  editMinute?: string;
};

const NewTimeBlock = ({
  title,
  imageSrc,
  imageAlt,
  editHour,
  editMinute,
  handleHour,
  handleMinute,
}: Props) => {
  return (
    <div className={styles.time}>
      <Image src={imageSrc} alt={imageAlt} width={30} height={30} />
      <Input
        type="number"
        name="hour"
        defaultValue={editHour}
        labelTitle={title}
        min={0}
        max={23}
        onChange={handleHour}
      />
      <span>:</span>
      <Input
        type="number"
        name="minutes"
        defaultValue={editMinute}
        min={0}
        max={59}
        onChange={handleMinute}
      />
    </div>
  );
};

export default NewTimeBlock;
