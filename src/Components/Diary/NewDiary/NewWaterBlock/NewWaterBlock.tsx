import Image from "next/image";
import { Button } from "@/components/UI";
import styles from "./NewWaterBlock.module.scss";

type Props = {
  glassOfWater: number | undefined;
  handleAddWater: () => void;
};

const NewWaterBlock = ({ glassOfWater, handleAddWater }: Props) => {
  if (glassOfWater === undefined) {
    return;
  }

  const renderedGlasses = Array.from({ length: glassOfWater }).map(
    (_, index) => (
      <Image
        key={index}
        src="/water-glass-color-icon.svg"
        width={35}
        height={35}
        alt="glass of water"
      />
    )
  );

  return (
    <div className={styles["glasses-container"]}>
      <Button
        type="small"
        onClick={handleAddWater}
        title="Add Glass of Water"
      />
      {glassOfWater === 0 && (
        <Image
          src="/empty-glass-icon.svg"
          width={35}
          height={35}
          alt="empty glass"
        />
      )}
      {glassOfWater > 0 && renderedGlasses}
    </div>
  );
};

export default NewWaterBlock;
