import { useRouter } from "next/navigation";
import Image from "next/image";
import { convertDateForUI } from "@/utility";
import { Button } from "@/components/UI";
import styles from "./NoDiaryBlock.module.scss";

type Props = {
  selectedDate: string;
};

export const NoDiaryBlock = ({ selectedDate }: Props) => {
  const router = useRouter();
  const handleAddDiary = () => {
    router.push("/diary/add");
  };

  return (
    <div className={styles["no-diary-block"]}>
      <Image
        src="/green_food.jpg"
        width={600}
        height={200}
        alt="food"
        className={styles.img}
      />
      <p className={styles.text}>
        You don`t have any notes for {convertDateForUI(selectedDate)}, but you
        can add some
      </p>
      <Button type="large" title="Add Diary" onClick={handleAddDiary} />
    </div>
  );
};
