import { useState } from "react";
import { convertDateForUI } from "@/utility";
import { Button } from "@/components/UI";
import { DayDiary } from "../DayDiary";
import { Analysis } from "../Analysis";
import styles from "./SelectedDiaries.module.scss";

type Props = {
  dateRange: string[];
  diaries: DayDiary[];
  onClickDate?: () => void;
};

export const SelectedDiaries = ({ dateRange, diaries, onClickDate }: Props) => {
  const [isAnalyze, setIsAnalyze] = useState(false);
  const [isShowOnlyMeals, setIsShowOnlyMeals] = useState(false);

  const handleAnalizeData = () => {
    setIsAnalyze((prev) => !prev);
  };

  const handleShowOnlyMeals = () => {
    setIsShowOnlyMeals((prev) => !prev);
  };

  const diariesBlock = diaries.map((diary) => (
    <DayDiary
      key={diary.id}
      data={diary}
      isShowOnlyMeals={isShowOnlyMeals}
      onClickDate={onClickDate}
    />
  ));

  return (
    <div className={styles["diaries-range-container"]}>
      <h3 className={styles.header}>
        {convertDateForUI(dateRange[0])} - {convertDateForUI(dateRange[1])}
      </h3>
      <div>
        <Button
          type="large"
          title={isShowOnlyMeals ? "Show All Info" : "Show Only Meals"}
          onClick={handleShowOnlyMeals}
        />
        <Button
          type="large"
          title={isAnalyze ? "Hide Statistics" : "Show Statistics"}
          onClick={handleAnalizeData}
          className={styles["statistics-btn"]}
        />
      </div>
      <div className={styles["diaries-block"]}>{diariesBlock}</div>
      {isAnalyze && <Analysis data={diaries} />}
    </div>
  );
};
