"use client";

import { signOut, useSession } from "next-auth/react";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { Button } from "../UI";
import styles from "./Navigation.module.scss";

const CALLBACK_URL = "http://localhost:3000/";

const LINKS_FOR_ALL = [
  {
    title: "Home",
    path: "/",
  },
  {
    title: "Recipes",
    path: "/recipes",
  },
];

const LINK_FOR_USER = {
  title: "Diary",
  path: "/diary",
};

const Navigation = () => {
  const { data: session } = useSession();

  const pathname = usePathname();

  const handleSignOut = () => {
    signOut({ callbackUrl: CALLBACK_URL });
  };

  const optionalNavigation = session && session.user && (
    <>
      <Link
        className={
          pathname === LINK_FOR_USER.path
            ? `${styles.link} ${styles.active}`
            : styles.link
        }
        href={LINK_FOR_USER.path}
      >
        {LINK_FOR_USER.title}
      </Link>
      <Button
        className={styles.auth}
        type="small"
        onClick={handleSignOut}
        title="Sign Out"
      />
    </>
  );

  const userInfo = session && session.user && (
    <div className={styles["user-name"]}>{session.user.name}</div>
  );

  const navigationLinks = LINKS_FOR_ALL.map((link) => {
    const classes =
      pathname === link.path
        ? `${styles.link} ${styles.active}`
        : `${styles.link}`;
    return (
      <Link key={link.title} href={link.path} className={classes}>
        {link.title}
      </Link>
    );
  });

  return (
    <header className={styles.header}>
      <nav className={styles.navigation}>
        <div className={styles["optional-links"]}>
          {navigationLinks}
          {optionalNavigation}
        </div>
        <div className={styles["additional-block"]}>{userInfo}</div>
      </nav>
    </header>
  );
};

export default Navigation;
