import { useState } from "react";
import { useSelector } from "react-redux";
import { RecipeState } from "@/app/GlobalRedux/store";
import { RecipeItemShort } from "@/components/Recipe/RecipeItemShort/RecipeItemShort";
import RecipeItem from "@/components/Recipe/RecipeItem/RecipeItem";
import styles from "./FavoriteRecipes.module.scss";

export const FavoriteRecipes = () => {
  const userRecipes = useSelector(
    (state: RecipeState) => state.recipe.userRecipes
  );

  const [userRecipe, setUserRecipe] = useState<RecipeLong>();

  const handleClickOnFavoriteRecipe = (recipeId: number) => {
    const recipe = userRecipes?.find((item) => item.id === recipeId);
    setUserRecipe(recipe);
  };

  const renderedUserRecipes =
    userRecipes &&
    userRecipes.length > 0 &&
    userRecipes.map((userRecipe) => (
      <RecipeItemShort
        key={userRecipe.id}
        recipe={userRecipe}
        isFavorite
        onClickFavoriteRecipe={handleClickOnFavoriteRecipe}
      />
    ));

  return (
    <section className={styles.main}>
      {userRecipes && (
        <div className={styles["recipes-container"]}>{renderedUserRecipes}</div>
      )}
      {userRecipe && <RecipeItem recipe={userRecipe} isFavorite />}
    </section>
  );
};
