import { ChangeEvent, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RecipeState, recipeActions } from "@/app/GlobalRedux/store";
import { getRecipes } from "@/lib";
import { Button, Input } from "@/components";
import { RecipeItemShort } from "../RecipeItemShort/RecipeItemShort";
import styles from "./FindRecipes.module.scss";

export const FindRecipes = () => {
  const dispatch = useDispatch();

  const recipes = useSelector((state: RecipeState) => state.recipe.recipes);
  const userIngredients = useSelector(
    (state: RecipeState) => state.recipe.userIngredients
  );

  const [isLoading, setIsLoading] = useState(false);

  const handleChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.currentTarget.value.trim() === "") {
      return;
    }
    dispatch(recipeActions.setUserIngredients(e.currentTarget.value));
  };

  const handleClearButton = () => {
    dispatch(recipeActions.setUserIngredients(""));
    dispatch(recipeActions.setRecipes(undefined));
  };

  const handleSearchButton = () => {
    if (!userIngredients) {
      return;
    }
    setIsLoading(true);
    getRecipes(userIngredients)
      .then((result) => {
        const fetchedRecipes = result.map((item: any) => ({
          id: item.id,
          title: item.title,
          image: item.image,
        }));
        dispatch(recipeActions.setRecipes(fetchedRecipes));
      })
      .then(() => setIsLoading(false));
  };

  const renderedRecipes =
    recipes &&
    recipes?.length > 0 &&
    recipes.map((recipe) => (
      <RecipeItemShort key={recipe.id} recipe={recipe} />
    ));

  return (
    <section className={styles.main}>
      <h1 className={styles.header}>
        Add ingredients and find the recipe you are looking for:
      </h1>
      <div className={styles["input-container"]}>
        <Input
          type="text"
          name="Find recipes"
          className={styles.input}
          onChange={handleChangeInput}
          defaultValue={userIngredients}
        />
        <Button
          type="medium"
          title="Clear"
          onClick={handleClearButton}
          disabled={isLoading}
          className={styles["clear-btn"]}
        />
        <Button
          type="medium"
          title="Search"
          onClick={handleSearchButton}
          disabled={isLoading}
        />
      </div>
      <div className={styles["recipes-container"]}>{renderedRecipes}</div>
    </section>
  );
};
