import Image from "next/image";
import styles from "./IngredientItem.module.scss";

export const IngredientItem = ({ name, amount, measure }: Ingredient) => {
  return (
    <div className={styles.container}>
      <div className={styles.text}>
        <Image src="/tick-mark-icon.svg" width={15} height={15} alt="check" />
        <p className={styles.name}>{name}</p>
      </div>
      <p className={styles.numbers}>
        <p className={styles.amount}>{amount}</p>
        <p className={styles.measure}>{measure}</p>
      </p>
    </div>
  );
};
