import Image from "next/image";
import { useSession } from "next-auth/react";
import { useState } from "react";
import { IngredientItem } from "../IngredientItem/IngredientItem";
import { Button } from "@/components/UI";
import { sendRecipe } from "@/lib";
import styles from "./RecipeItem.module.scss";

type Props = {
  recipe: RecipeLong;
  isFavorite?: boolean;
};

const RecipeItem = ({ recipe, isFavorite }: Props) => {
  const { data: session } = useSession();

  const [isAdded, setIsAdded] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const arrayOfInstructions =
    recipe.instructions && recipe.instructions.split(".");

  const handleClickButton = () => {
    const currentRecipe: RecipeLong = {
      ...recipe,
      userId: session?.user.id,
    };

    setIsLoading(true);
    sendRecipe(currentRecipe)
      .then(() => setIsAdded(true))
      .then(() => setIsLoading(false));
  };

  const sendButtonTitle = isAdded ? "in favorite" : "add to favorite";

  const renderedInstructions =
    arrayOfInstructions &&
    arrayOfInstructions.map((item, index) =>
      item.length > 0 ? (
        <li className={styles["instruction-item"]} key={index}>
          {item}
        </li>
      ) : undefined
    );

  const renderedIngredients = recipe.ingredients?.map((ingredient) => (
    <IngredientItem key={ingredient.id} {...ingredient} />
  ));

  return (
    <div className={styles["main-container"]}>
      <Image
        className={styles.pin}
        src="/pin-fill.svg"
        width={30}
        height={30}
        alt="pin"
      />
      {!isFavorite && session && session.user && (
        <Button
          type="small"
          title={sendButtonTitle}
          onClick={handleClickButton}
          disabled={isLoading || isAdded}
          imageSrc={isAdded ? "tick-icon.svg" : undefined}
        />
      )}
      <h2 className={styles.title}>{recipe.title}</h2>
      {Array.isArray(arrayOfInstructions) && (
        <ol className={styles.instructions}>{renderedInstructions}</ol>
      )}
      <div className={styles["image-container"]}>
        <Image
          src={recipe.image}
          width={300}
          height={190}
          alt="recipe"
          className={styles.image}
        />
      </div>
      <div className={styles.time}>
        <Image src="/clock.svg" width={25} height={25} alt="clock" />
        <p className={styles.minutes}>{recipe.readyInMinutes}</p>
        <p>min</p>
      </div>
      <div className={styles["ingredients-container"]}>
        <h2 className={styles.header}>Ingredients:</h2>
        {renderedIngredients}
      </div>
    </div>
  );
};

export default RecipeItem;
