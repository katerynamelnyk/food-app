import Image from "next/image";
import { useRouter } from "next/navigation";
import styles from "./RecipeItemShort.module.scss";

type Props = {
  recipe: RecipeShort;
  isFavorite?: boolean;
  onClickFavoriteRecipe?: (id: number) => void;
};

export const RecipeItemShort = ({
  recipe,
  isFavorite,
  onClickFavoriteRecipe,
}: Props) => {
  const router = useRouter();

  const handleRecipeButton = (id: number) => {
    if (!id) {
      return;
    }
    isFavorite && onClickFavoriteRecipe && onClickFavoriteRecipe(id);
    !isFavorite && router.push(`/recipes/${id.toString()}`);
  };

  return (
    <div
      className={styles.container}
      onClick={() => handleRecipeButton(recipe.id)}
    >
      {isFavorite && (
        <Image
          src="/red-heart-icon.svg"
          width={20}
          height={20}
          alt="like"
          className={styles.like}
        />
      )}
      <p className={styles.title}>{recipe.title}</p>
      <div className={styles["image-container"]}>
        <Image
          src={recipe.image}
          width={300}
          height={190}
          alt="recipe"
          className={styles.image}
        />
      </div>
    </div>
  );
};
