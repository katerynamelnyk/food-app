import Image from "next/image";
import styles from "./Button.module.scss";

type Props = {
  type: "small" | "medium" | "large" | "empty";
  onClick?: (id?: string) => void;
  children?: React.ReactNode;
  title?: string;
  className?: string;
  disabled?: boolean;
  imageSrc?: string;
};

export const Button = (props: Props) => {
  const typeClass =
    props.type === "small"
      ? `${styles.small}`
      : props.type === "medium"
      ? `${styles.medium}`
      : props.type === "large"
      ? `${styles.large}`
      : `${styles.empty}`;

  const customClasses = props.className
    ? `${typeClass} ${props.className}`
    : typeClass;

  const disabledClass = props.disabled && `${styles.disabled}`;

  return (
    <button
      className={`${styles.button} ${customClasses} ${disabledClass}`}
      onClick={(e) => {
        e.preventDefault();
        props.onClick && props.onClick();
      }}
      disabled={props.disabled}
    >
      {props.imageSrc && (
        <Image
          src={`/${props.imageSrc}`}
          width={15}
          height={15}
          alt={props.imageSrc}
        />
      )}
      {props?.title}
      {props.children}
    </button>
  );
};
