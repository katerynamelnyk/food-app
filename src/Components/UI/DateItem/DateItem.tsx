import {
  convertDateToDay,
  convertDateToMonth,
  convertDateToWeekDay,
} from "@/utility";
import { Button } from "../Button";
import styles from "./DateItem.module.scss";

type Props = {
  date: string;
  onClickDate?: () => void;
};

export const DateItem = ({ date, onClickDate }: Props) => {
  return (
    <div className={styles.date}>
      {onClickDate ? (
        <Button
          type="empty"
          onClick={onClickDate}
          disabled={!onClickDate}
          className={styles["date-btn"]}
        >
          <p className={styles.day}>{convertDateToDay(date)}</p>
          <p>{convertDateToMonth(date)}</p>
          <p className={styles["week-day"]}>{convertDateToWeekDay(date)}</p>
        </Button>
      ) : (
        <div className={styles.container}>
          <p className={styles.day}>{convertDateToDay(date)}</p>
          <p>{convertDateToMonth(date)}</p>
          <p className={styles["week-day"]}>{convertDateToWeekDay(date)}</p>
        </div>
      )}
    </div>
  );
};
