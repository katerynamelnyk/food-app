import { ChangeEvent } from "react";
import styles from "./DropDown.module.scss";
import Image from "next/image";

type Props = {
  name: string;
  title: string;
  options: string[];
  value: string;
  onChange?: (e: ChangeEvent<HTMLSelectElement>) => void;
};

export const DropDown = (props: Props) => {
  const renderedOptions = props.options.map((option, i) => (
    <option key={i} value={option} className={styles.option}>
      {option}
    </option>
  ));

  return (
    <div className={styles.container}>
      <label htmlFor={props.name} className={styles.label}>
        {props.title}
      </label>
      <select
        id={props.name}
        className={styles.option}
        onChange={props?.onChange}
        value={props.value}
      >
        {renderedOptions}
      </select>
    </div>
  );
};
