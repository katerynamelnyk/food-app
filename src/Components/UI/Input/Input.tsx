import React, { ChangeEvent, Ref } from "react";
import styles from "./Input.module.scss";

type Props = {
  name: string;
  type: "number" | "text" | "date" | "checkbox" | "email" | "password";
  labelTitle?: string;
  defaultValue?: string | number;
  isChecked?: boolean;
  min?: number;
  max?: number;
  className?: string;
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
};

export const Input = React.forwardRef<HTMLInputElement, Props>(
  (props: Props, ref?: Ref<HTMLInputElement>) => {
    const classes = `${props.className} ${styles.input}`
    return (
      <div className={styles.container}>
        {props.labelTitle && (
          <label htmlFor={props.name} className={styles.label}>
            {props.labelTitle}
          </label>
        )}
        <input
          id={props.name}
          ref={ref}
          type={props.type}
          value={props?.defaultValue}
          className={classes}
          onChange={props?.onChange}
          checked={props?.isChecked}
          min={props.min}
          max={props.max}
        />
      </div>
    );
  }
);

Input.displayName = "Input";
