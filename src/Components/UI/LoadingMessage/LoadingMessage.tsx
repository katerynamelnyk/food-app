import styles from "./LoadingMessage.module.scss";

export const LoadingMessage = () => {
  return <div className={styles.loading}>Just a sec...</div>;
};
