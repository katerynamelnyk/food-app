"use client";

import { ChangeEvent, useState } from "react";
import { useRouter } from "next/navigation";
import { useSession } from "next-auth/react";
import { useSelector } from "react-redux";
import { sendDiary } from "@/lib";
import NewMealBlock from "@/components/Diary/NewDiary/NewMealBlock/NewMealBlock";
import NewWaterBlock from "@/components/Diary/NewDiary/NewWaterBlock/NewWaterBlock";
import NewCoffeeBlock from "@/components/Diary/NewDiary/NewCoffeeBlock/NewCoffeeBlock";
import NewStepsAndWorkoutBlock from "@/components/Diary/NewDiary/NewStepsAndWorkoutBlock/NewStepsAndWorkoutBlock";
import NewTimeBlock from "@/components/Diary/NewDiary/NewTimeBlock/NewTimeBlock";
import NewMoodBlock from "@/components/Diary/NewDiary/NewMoodBlock/NewMoodBlock";
import { MoodType } from "@prisma/client";
import { DiaryState } from "@/app/GlobalRedux/store";
import { convertStringToMood, convertTimeForDB } from "@/utility";
import { Button, DateItem } from "@/components";
import styles from "./AddDiaryPage.module.scss";

const AddDiaryPage = () => {
  const { data: session } = useSession();
  const router = useRouter();
  const selectedDate = useSelector(
    (state: DiaryState) => state.diary.selectedDate
  );

  const [mealBlocks, setMealBlocks] = useState<JSX.Element[]>([]);
  const [mealData, setMealData] = useState<MealItem[]>([]);
  const [glassOfWater, setGlassOfWater] = useState(0);
  const [cupOfCoffee, setCupOfCoffee] = useState(0);
  const [steps, setSteps] = useState(0);
  const [isWorkout, setIsWorkout] = useState(false);

  const [wakeUpHour, setWakeUpHour] = useState("");
  const [wakeUpMinute, setWakeUpMinute] = useState("");

  const [goSleepHour, setGoSleepHour] = useState<string | null>(null);
  const [goSleepMinute, setGoSleepMinute] = useState<string | null>(null);

  const [mood, setMood] = useState<MoodType>(MoodType.neutral);

  const [isLoading, setIsLoading] = useState(false);

  const handleWakeUpHour = (e: ChangeEvent<HTMLInputElement>) => {
    setWakeUpHour(e.currentTarget.value);
  };

  const handleWakeUpMinute = (e: ChangeEvent<HTMLInputElement>) => {
    setWakeUpMinute(e.currentTarget.value);
  };

  const handleGoSleepHour = (e: ChangeEvent<HTMLInputElement>) => {
    setGoSleepHour(e.currentTarget.value);
  };

  const handleGoSleepMinute = (e: ChangeEvent<HTMLInputElement>) => {
    setGoSleepMinute(e.currentTarget.value);
  };

  const handleMood = (e: ChangeEvent<HTMLSelectElement>) => {
    const convertedValue = convertStringToMood(e.currentTarget.value);
    setMood(convertedValue);
  };

  const handleSteps = (e: ChangeEvent<HTMLInputElement>) => {
    setSteps(Number(e.currentTarget.value));
  };

  const handleWorkout = (e: ChangeEvent<HTMLInputElement>) => {
    setIsWorkout((prev) => !prev);
  };

  const handleAddWater = () => {
    setGlassOfWater((prev) => prev + 1);
  };

  const handleAddCoffee = () => {
    setCupOfCoffee((prev) => prev + 1);
  };

  const handleSaveMealData = (data: MealItem) => {
    setMealData((prev) => [...prev, data]);
  };

  const handleAddMeal = () => {
    setMealBlocks((prevBlocks) => [
      ...prevBlocks,
      <NewMealBlock onSaveMeal={handleSaveMealData} key={prevBlocks.length} />,
    ]);
  };

  const handleSubmitAll = () => {
    if (!session || !session.user.id) {
      return;
    }

    const convertedWakeUpTime = convertTimeForDB(wakeUpHour, wakeUpMinute);
    const convertedGoSleepTime =
      goSleepHour !== null && goSleepMinute !== null
        ? convertTimeForDB(goSleepHour, goSleepMinute)
        : null;

    const data: DayDiary = {
      id: Math.random().toString(),
      userId: session.user.id,
      date: selectedDate,
      mealItems: mealData,
      glassOfWater: glassOfWater || 0,
      workOut: isWorkout,
      steps,
      cupOfCoffee,
      wakeUpTime: convertedWakeUpTime,
      goSleepTime: convertedGoSleepTime,
      mood,
    };
    console.log(data);

    setIsLoading(true);
    sendDiary(data)
      .then(() => setIsLoading(false))
      .then(() => router.back());
  };

  return (
    <div className={styles.main}>
      <Button
        type="small"
        title="BACK"
        onClick={() => router.back()}
        className={styles["btn-back"]}
      />
      <div className={styles["header-block"]}>
        <h1>Add information about your day</h1>
        <DateItem date={selectedDate} />
      </div>
      <form className={styles["form-container"]}>
        <div className={styles["inputs-container"]}>
          <NewTimeBlock
            title="Wake Up Time"
            imageSrc="/wake-up-icon.svg"
            imageAlt="wake up"
            handleHour={handleWakeUpHour}
            handleMinute={handleWakeUpMinute}
          />
          <NewCoffeeBlock
            handleAddCoffee={handleAddCoffee}
            cupOfCoffee={cupOfCoffee}
          />
          <NewWaterBlock
            glassOfWater={glassOfWater}
            handleAddWater={handleAddWater}
          />
          <NewStepsAndWorkoutBlock
            handleSteps={handleSteps}
            handleWorkout={handleWorkout}
          />
          <NewTimeBlock
            title="Go To Sleep Time"
            imageSrc="/snooze-zzz-icon.svg"
            imageAlt="go sleep"
            handleHour={handleGoSleepHour}
            handleMinute={handleGoSleepMinute}
          />
          <NewMoodBlock mood={mood} handleMood={handleMood} />
          {mealBlocks}
          <Button
            type="medium"
            title={mealBlocks.length === 0 ? "Add Meal" : "Add One More Meal"}
            onClick={handleAddMeal}
          />
          <div className={styles["btn-container"]}>
            <Button
              type="large"
              title={!isLoading ? "Submit" : "Loading..."}
              onClick={handleSubmitAll}
              disabled={mealData.length === 0 || isLoading}
            />
          </div>
        </div>
      </form>
    </div>
  );
};

export default AddDiaryPage;
