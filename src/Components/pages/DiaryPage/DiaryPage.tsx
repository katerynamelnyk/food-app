"use client";

import { useState, useEffect } from "react";
import { useSession } from "next-auth/react";
import { useSelector } from "react-redux";
import { DiaryState } from "@/app/GlobalRedux/store";
import {
  deleteDayDiary,
  getSelectedDiaries,
  getDatesWithDiary,
  getDiary,
} from "@/lib";
import {
  Button,
  LoadingMessage,
  DayDiary,
  DiaryCalendar,
  NoDiaryBlock,
  SelectedDiaries,
} from "@/components";
import styles from "./DiaryPage.module.scss";

const DiaryPage = () => {
  const { data: session } = useSession();
  const userId = session && session.user.id;

  const selectedDate = useSelector(
    (state: DiaryState) => state.diary.selectedDate
  );

  const [diary, setDiary] = useState<DayDiary>();
  const [dates, setDates] = useState<Date[]>([]);

  const [isDateRange, setIsDateRange] = useState(false);
  const [dateRange, setDateRange] = useState<string[]>([]);
  const [diaries, setDiaries] = useState<DayDiary[]>([]);

  const [isDeletedDayDiary, setIsDeletedDayDiary] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (!userId) {
      return;
    }
    setIsLoading(true);
    getDiary(userId, selectedDate)
      .then(setDiary)
      .then(() => setIsDeletedDayDiary(false))
      .then(() => setIsLoading(false));
  }, [selectedDate, isDeletedDayDiary]);

  useEffect(() => {
    if (!userId) {
      return;
    }
    setIsLoading(true);
    getDatesWithDiary(userId)
      .then(setDates)
      .then(() => setIsLoading(false));
  }, [isDeletedDayDiary]);

  useEffect(() => {
    if (dateRange.length === 0 || !userId) {
      return;
    }
    setIsLoading(true);
    getSelectedDiaries(userId, dateRange)
      .then(setDiaries)
      .then(() => setIsLoading(false));
  }, [dateRange]);

  const handleDateRange = (dates: string[]) => {
    setDateRange(dates);
  };

  const toggleSelectDateRange = () => {
    setIsDateRange((prev) => !prev);
  };

  const handleDeleteDayDiary = (id?: string) => {
    if (!id) {
      return;
    }
    setIsLoading(true);
    deleteDayDiary(id)
      .then(() => setIsDeletedDayDiary(true))
      .then(() => setIsLoading(false));
  };

  return (
    <div className={styles.main}>
      <h2 className={styles["header-main"]}>My Health And Food Diary</h2>
      <Button
        type="small"
        title={isDateRange ? "Select Date" : "Select Date Range"}
        onClick={toggleSelectDateRange}
      />
      <DiaryCalendar
        isDateRange={isDateRange}
        dates={dates}
        handleDateRange={handleDateRange}
      />
      {isLoading && <LoadingMessage />}
      {!isLoading && !isDateRange && !diary && (
        <NoDiaryBlock selectedDate={selectedDate} />
      )}
      {!isLoading && !isDateRange && diary && (
        <DayDiary
          data={diary}
          isShowButtons={true}
          onDelete={handleDeleteDayDiary}
        />
      )}
      {!isLoading && isDateRange && diaries.length > 0 && (
        <SelectedDiaries
          dateRange={dateRange}
          diaries={diaries}
          onClickDate={toggleSelectDateRange}
        />
      )}
    </div>
  );
};

export default DiaryPage;
