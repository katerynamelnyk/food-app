"use client";

import { useState, ChangeEvent } from "react";
import { useRouter } from "next/navigation";
import { useSelector } from "react-redux";
import { DiaryState } from "@/app/GlobalRedux/store";
import { sendMealItems, sendFoodItems, updateDiary } from "@/lib";
import { MoodType } from "@prisma/client";
import { Button, DateItem } from "@/components";
import {
  convertHour,
  convertMinute,
  convertStringToMood,
  convertTimeForDB,
} from "@/utility";
import NewMealBlock from "@/components/Diary/NewDiary/NewMealBlock/NewMealBlock";
import NewWaterBlock from "@/components/Diary/NewDiary/NewWaterBlock/NewWaterBlock";
import NewStepsAndWorkoutBlock from "@/components/Diary/NewDiary/NewStepsAndWorkoutBlock/NewStepsAndWorkoutBlock";
import NewCoffeeBlock from "@/components/Diary/NewDiary/NewCoffeeBlock/NewCoffeeBlock";
import NewTimeBlock from "@/components/Diary/NewDiary/NewTimeBlock/NewTimeBlock";
import NewMoodBlock from "@/components/Diary/NewDiary/NewMoodBlock/NewMoodBlock";
import styles from "./EditDiaryPage.module.scss";

const EditDiaryPage = () => {
  const router = useRouter();

  const diaryToEdit = useSelector(
    (state: DiaryState) => state.diary.diaryToEdit
  ) as DayDiary;

  const [mealData, setMealData] = useState<MealItem[]>(diaryToEdit?.mealItems);
  const [glassOfWater, setGlassOfWater] = useState<number>(
    diaryToEdit?.glassOfWater
  );
  const [cupOfCoffee, setCupOfCoffee] = useState<number>(
    diaryToEdit?.cupOfCoffee
  );
  const [steps, setSteps] = useState<number>(diaryToEdit?.steps);
  const [isWorkout, setIsWorkout] = useState<boolean>(diaryToEdit?.workOut);
  const [wakeUpHour, setWakeUpHour] = useState(
    convertHour(diaryToEdit?.wakeUpTime)
  );
  const [wakeUpMinute, setWakeUpMinute] = useState(
    convertMinute(diaryToEdit?.wakeUpTime)
  );
  const [goSleepHour, setGoSleepHour] = useState<string>(
    diaryToEdit?.goSleepTime ? convertHour(diaryToEdit?.goSleepTime) : ""
  );
  const [goSleepMinute, setGoSleepMinute] = useState<string>(
    diaryToEdit?.goSleepTime ? convertMinute(diaryToEdit?.goSleepTime) : ""
  );
  const [mood, setMood] = useState<MoodType>(diaryToEdit?.mood);

  const [isLoading, setIsLoading] = useState(false);

  const [addedMealBlocks, setAddedMealBlocks] = useState<JSX.Element[]>([]);
  const [addedMealData, setAddedMealData] = useState<MealItem[]>([]);
  const [addedFoodData, setAddedFoodData] = useState<Food[]>([]);

  const handleWakeUpHour = (e: ChangeEvent<HTMLInputElement>) => {
    setWakeUpHour(e.currentTarget.value);
  };

  const handleWakeUpMinutes = (e: ChangeEvent<HTMLInputElement>) => {
    setWakeUpMinute(e.currentTarget.value);
  };

  const handleGoSleepHour = (e: ChangeEvent<HTMLInputElement>) => {
    setGoSleepHour(e.currentTarget.value);
  };

  const handleGoSleepMinutes = (e: ChangeEvent<HTMLInputElement>) => {
    setGoSleepMinute(e.currentTarget.value);
  };

  const handleMood = (e: ChangeEvent<HTMLSelectElement>) => {
    const convertedValue = convertStringToMood(e.currentTarget.value);
    setMood(convertedValue);
  };

  const handleAddCoffee = () => {
    setCupOfCoffee((prevAmount) => prevAmount + 1);
  };

  const handleAddWater = () => {
    setGlassOfWater((prevAmount) => prevAmount + 1);
  };

  const handleSteps = (e: ChangeEvent<HTMLInputElement>) => {
    setSteps(Number(e.currentTarget.value));
  };

  const handleWorkout = (e: ChangeEvent<HTMLInputElement>) => {
    setIsWorkout((prev) => !prev);
  };

  const handleSaveEditedMealBlocks = (
    mealData: MealItem,
    foodData?: Food[]
  ) => {
    setMealData((prev) =>
      prev.map((item) => (item.id === mealData.id ? mealData : item))
    );
    foodData && setAddedFoodData((prevData) => [...prevData, ...foodData]);
  };

  const handleSaveAddedMealBlocks = (data: MealItem) => {
    setAddedMealData((prevData) => [...prevData, data]);
  };

  const mealBlocks = diaryToEdit?.mealItems.map((mealItem) => (
    <NewMealBlock
      key={mealItem.id}
      onSaveMeal={handleSaveEditedMealBlocks}
      editedData={mealItem}
    />
  ));

  const handleAddMeal = () => {
    setAddedMealBlocks((prevBlocks) => [
      ...prevBlocks,
      <NewMealBlock
        onSaveMeal={handleSaveAddedMealBlocks}
        key={prevBlocks.length}
      />,
    ]);
  };

  const handleSubmitAll = () => {
    if (!diaryToEdit) {
      return;
    }

    const convertedWakeUpTime = convertTimeForDB(wakeUpHour, wakeUpMinute);
    const convertedGoSleepTime =
      goSleepHour !== null && goSleepMinute !== null
        ? convertTimeForDB(goSleepHour, goSleepMinute)
        : null;

    const data: DayDiary = {
      id: diaryToEdit.id,
      userId: diaryToEdit.userId,
      date: diaryToEdit.date,
      mealItems: mealData,
      glassOfWater: glassOfWater || 0,
      cupOfCoffee: cupOfCoffee || 0,
      workOut: isWorkout || false,
      steps: steps || 0,
      wakeUpTime: convertedWakeUpTime,
      goSleepTime: convertedGoSleepTime,
      mood,
    };

    console.log(data);

    setIsLoading(true);

    addedMealData.length > 0 && sendMealItems(addedMealData, diaryToEdit.id);
    addedFoodData.length > 0 && sendFoodItems(addedFoodData);
    updateDiary(data)
      .then(() => setIsLoading(false))
      .then(() => router.back());
  };

  return (
    <div className={styles.main}>
      <Button
        type="small"
        title="Back"
        onClick={() => router.back()}
        className={styles["btn-back"]}
      />
      {diaryToEdit && (
        <div className={styles["data-container"]}>
          <div className={styles["header-block"]}>
            <h1>You can edit your diary if you need</h1>
            <DateItem date={diaryToEdit.date} />
          </div>
          <form className={styles.form}>
            {mealBlocks}
            {addedMealBlocks.length > 0 && addedMealBlocks}
            <div className={styles["inputs-container"]}>
              <Button
                type="medium"
                title="Add One More Meal"
                onClick={handleAddMeal}
              />
              <NewTimeBlock
                title="Wake Up Time"
                imageSrc="/wake-up-icon.svg"
                imageAlt="wake up"
                editHour={wakeUpHour}
                editMinute={wakeUpMinute}
                handleHour={handleWakeUpHour}
                handleMinute={handleWakeUpMinutes}
              />
              <NewCoffeeBlock
                handleAddCoffee={handleAddCoffee}
                cupOfCoffee={cupOfCoffee}
              />
              <NewWaterBlock
                glassOfWater={glassOfWater}
                handleAddWater={handleAddWater}
              />
              <NewStepsAndWorkoutBlock
                handleSteps={handleSteps}
                handleWorkout={handleWorkout}
                editedIsWorkout={isWorkout}
                editedSteps={steps}
              />
              <NewTimeBlock
                title="Go To Sleep Time"
                imageSrc="/snooze-zzz-icon.svg"
                imageAlt="go sleep"
                editHour={goSleepHour !== null ? goSleepHour : undefined}
                editMinute={goSleepMinute !== null ? goSleepMinute : undefined}
                handleHour={handleGoSleepHour}
                handleMinute={handleGoSleepMinutes}
              />
              <NewMoodBlock mood={mood} handleMood={handleMood} />
              <div className={styles["btn-container"]}>
                <Button
                  type="large"
                  title={!isLoading ? "Done" : "Loading..."}
                  onClick={handleSubmitAll}
                  disabled={mealData?.length === 0 || isLoading}
                />
              </div>
            </div>
          </form>
        </div>
      )}
    </div>
  );
};

export default EditDiaryPage;
