"use client";

import Image from "next/image";
import { signIn, useSession } from "next-auth/react";
import { useRouter } from "next/navigation";
import { Button } from "@/components";
import styles from "./HomePage.module.scss";

const IMAGE_HEIGHT = 220;
const IMAGE_WIDTH = 234;

const CALLBACK_URL = "http://localhost:3000/";

const HomePage = () => {
  const { data: session } = useSession();
  const router = useRouter();

  const handleSignIn = () => {
    signIn(undefined, { callbackUrl: CALLBACK_URL });
  };

  const handleSignUp = () => {
    router.push("/signUp");
  };

  return (
    <main className={styles.main}>
      <div className={styles.container}>
        <div className={styles.logo}>
          <p className={styles["logo-first"]}>Smart</p>
          <p className={styles["logo-second"]}>Eating</p>
        </div>
        <Image
          src="/pictures/1.jpg"
          alt="food"
          height={IMAGE_HEIGHT}
          width={IMAGE_WIDTH}
        />
        <Image
          src="/pictures/2.jpg"
          alt="food"
          height={IMAGE_HEIGHT}
          width={IMAGE_WIDTH}
        />
        <Image
          src="/pictures/3.jpg"
          alt="food"
          height={IMAGE_HEIGHT}
          width={IMAGE_WIDTH}
        />
        <div className={styles.text}>
          <p>Healthy Life</p>
        </div>
        <Image
          src="/pictures/4.jpg"
          alt="food"
          height={IMAGE_HEIGHT}
          width={IMAGE_WIDTH}
        />
        <div className={styles["button-container"]}>
          {!(session && session.user) && (
            <>
              <Button
                type="medium"
                onClick={handleSignIn}
                className={styles["signin-button"]}
                title="sign in"
              />
              <Button
                type="medium"
                onClick={handleSignUp}
                className={styles["signin-button"]}
                title="sign up"
              />
            </>
          )}
        </div>
      </div>
      <div className={styles.remark}>You are what you eat.</div>
    </main>
  );
};

export default HomePage;
