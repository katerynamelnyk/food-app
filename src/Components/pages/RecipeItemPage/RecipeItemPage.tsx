"use client";

import { useEffect, useState } from "react";
import { useParams, useRouter } from "next/navigation";
import { getRecipeById } from "@/lib";
import { Button, LoadingMessage } from "@/components";
import RecipeItem from "@/components/Recipe/RecipeItem/RecipeItem";
import styles from "./RecipeItemPage.module.scss";

const RecipeItemPage = () => {
  const { recipeId } = useParams();
  const router = useRouter();

  const [recipe, setRecipe] = useState<RecipeLong>();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    getRecipeById(Number(recipeId))
      .then((result) => {
        const ingredients: Ingredient[] = result.extendedIngredients.map(
          (item: any) => ({
            id: item.id,
            name: item.name,
            amount: item.amount,
            measure: item.unit,
          })
        );
        setRecipe({
          id: result.id,
          title: result.title,
          readyInMinutes: result.readyInMinutes,
          image: result.image,
          instructions: result.instructions,
          vegan: result.vegan,
          dairyFree: result.dairyFree,
          veryHealthy: result.veryHealthy,
          ingredients,
        });
      })
      .then(() => setIsLoading(false));
  }, [recipeId]);

  const handleBackButton = () => {
    router.back();
  };

  return (
    <div className={styles.main}>
      <Button
        type="small"
        title="back"
        onClick={handleBackButton}
        className={styles["btn-back"]}
      />
      {isLoading && <LoadingMessage />}
      {!isLoading && recipe && <RecipeItem recipe={recipe} />}
    </div>
  );
};

export default RecipeItemPage;
