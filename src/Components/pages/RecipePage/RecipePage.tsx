"use client";

import { useState } from "react";
import { useDispatch } from "react-redux";
import { useSession } from "next-auth/react";
import { recipeActions } from "@/app/GlobalRedux/store";
import { getRecipesByUserId } from "@/lib";
import { Button } from "@/components";
import { FavoriteRecipes } from "@/components/Recipe/FavoriteRecipes/FavoriteRecipes";
import { FindRecipes } from "@/components/Recipe/FindRecipes/FindRecipes";
import styles from "./RecipePage.module.scss";

type Tabs = "Favorite" | "Search";

const RecipePage = () => {
  const dispatch = useDispatch();
  const { data: session } = useSession();

  const [isLoading, setIsLoading] = useState(false);
  const [isActiveTab, setIsActiveTab] = useState<Tabs>("Search");

  const handleFindButton = () => {
    setIsActiveTab("Search");
  };

  const handleFavoriteButton = () => {
    const userId = session?.user.id;
    if (!userId) {
      return;
    }
    setIsLoading(true);
    getRecipesByUserId(userId)
      .then((data) => dispatch(recipeActions.setUserRecipes(data)))
      .then(() => setIsLoading(false));
    setIsActiveTab("Favorite");
  };

  return (
    <section className={styles.main}>
      <div className={styles["input-container"]}>
        <Button
          type="large"
          title="find recipes by ingredients"
          onClick={handleFindButton}
          disabled={isLoading}
          className={isActiveTab === "Search" ? `${styles.active}` : ""}
        />
        {session && session.user && (
          <Button
            type="large"
            title="favorite recipes"
            onClick={handleFavoriteButton}
            disabled={isLoading}
            className={isActiveTab === "Favorite" ? `${styles.active}` : ""}
          />
        )}
      </div>
      {isActiveTab === "Favorite" && <FavoriteRecipes />}
      {isActiveTab === "Search" && <FindRecipes />}
    </section>
  );
};

export default RecipePage;
