import RecipeItemPage from "@/components/pages/RecipeItemPage/RecipeItemPage";

const RecipeItem = () => {
  return <RecipeItemPage />;
};

export default RecipeItem;
