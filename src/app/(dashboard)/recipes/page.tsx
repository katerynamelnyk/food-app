import RecipePage from "@/components/pages/RecipePage/RecipePage";

const Recipes = () => {
  return <RecipePage />;
};

export default Recipes;
