"use client";

import { useRef, useState } from "react";
import { signIn } from "next-auth/react";
import { useRouter } from "next/navigation";
import { sendUser, User } from "@/lib";
import { Button, Input, LoadingMessage } from "@/components";
import styles from "./page.module.scss";

const CALLBACK_URL = "http://localhost:3000/";

const SignUp = () => {
  const router = useRouter();

  const [isLoading, setIsLoading] = useState(false);
  const userNameRef = useRef<HTMLInputElement>(null);
  const userEmailRef = useRef<HTMLInputElement>(null);
  const userPassword = useRef<HTMLInputElement>(null);

  const handleSendForm = () => {
    if (
      userNameRef.current?.value === undefined ||
      userEmailRef.current?.value === undefined ||
      userPassword.current?.value === undefined
    ) {
      return;
    }

    const createdUser: User = {
      name: userNameRef.current?.value,
      email: userEmailRef.current?.value,
      password: userPassword.current?.value,
    };

    setIsLoading(true);
    sendUser(createdUser)
      .then(() => setIsLoading(false))
      .then(() => signIn(undefined, { callbackUrl: CALLBACK_URL }));
  };

  const handleCancelForm = () => {
    router.back();
  };

  return (
    <div className={styles.main}>
      <h2>Create Your Account</h2>
      {isLoading && <LoadingMessage />}
      {!isLoading && (
        <form>
          <Input
            type="text"
            name="user name"
            labelTitle="Name"
            ref={userNameRef}
          />
          <Input
            type="email"
            name="email"
            labelTitle="Email"
            ref={userEmailRef}
          />
          <Input
            type="password"
            name="password"
            labelTitle="Password"
            ref={userPassword}
          />
          <div>
            <Button type="small" title="Cancel" onClick={handleCancelForm} />
            <Button type="small" title="Submit" onClick={handleSendForm} />
          </div>
        </form>
      )}
    </div>
  );
};

export default SignUp;
