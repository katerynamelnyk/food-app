"use client";

import { configureStore, createSlice } from "@reduxjs/toolkit";
import { convertDateToISOString } from "@/utility";

type InitialDiary = {
  selectedDate: string;
  diaryToEdit?: DayDiary;
};

const initialDiaryState: InitialDiary = {
  selectedDate: convertDateToISOString(new Date()), //current Date as ISOString
};

const diarySlice = createSlice({
  name: "diary",
  initialState: initialDiaryState,
  reducers: {
    setSelectedDate: (state, action) => {
      state.selectedDate = action.payload;
    },
    setDiaryToEdit: (state, action) => {
      state.diaryToEdit = action.payload;
    },
  },
});

type InitialRecipe = {
  recipes?: RecipeShort[];
  userIngredients?: string;
  userRecipes?: RecipeLong[];
};

const initialRecipeState: InitialRecipe = {};

const recipeSlice = createSlice({
  name: "recipe",
  initialState: initialRecipeState,
  reducers: {
    setRecipes: (state, action) => {
      state.recipes = action.payload;
    },
    setUserIngredients: (state, action) => {
      state.userIngredients = action.payload;
    },
    setUserRecipes: (state, action) => {
      state.userRecipes = action.payload;
    }
  },
});

export const diaryActions = diarySlice.actions;

export const recipeActions = recipeSlice.actions;

export const store = configureStore({
  reducer: {
    diary: diarySlice.reducer,
    recipe: recipeSlice.reducer,
  },
});

export type DiaryState = ReturnType<typeof store.getState>;

export type RecipeState = ReturnType<typeof store.getState>;
