import prisma from "@/lib/prisma";

export const GET = async (request: Request) => {
  const url = request.url;
  const urlObject = new URL(url as string);
  const params = new URLSearchParams(urlObject.search);

  const userId = params.get("userId");

  const diary = await prisma.dayDiary.findMany({
    where: {
      userId: parseInt(userId as string),
    },
  });

  const dates = diary.map(item => item.date);

  return new Response(JSON.stringify(dates), {
    status: 200,
  });
};