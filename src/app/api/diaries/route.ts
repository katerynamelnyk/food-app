import prisma from "@/lib/prisma";
import { convertDateForDB, sortDiary } from "@/utility";

export const GET = async (request: Request) => {
  const url = request.url;
  const urlObject = new URL(url as string);
  const params = new URLSearchParams(urlObject.search);

  const userId = params.get("userId");
  const startDate = params.get("startDate");
  const endDate = params.get("endDate");

  if (!startDate || !endDate || userId === null) {
    return (
      new Response("There is no data!"),
      {
        status: 500,
      }
    );
  }

  const convertedStartDate = convertDateForDB(startDate);
  const convertedEndDate = convertDateForDB(endDate);

  //get diary for a specific user and of a selected date range, including all subitems(mealitems, food)
  const diaries = await prisma.dayDiary.findMany({
    where: {
      userId: parseInt(userId),
      date: {
        gte: convertedStartDate,
        lte: convertedEndDate,
      },
    },
    include: {
      mealItems: {
        include: {
          food: true,
        },
      },
    },
  });

  diaries.forEach((diary) => sortDiary(diary));

  diaries.sort((a, b) => new Date(a.date).getTime() - new Date(b.date).getTime());

  return new Response(JSON.stringify(diaries), {
    status: 200,
  });
};
