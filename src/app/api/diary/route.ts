import prisma from "@/lib/prisma";
import { convertDateForDB } from "@/utility";

type PostRequestBody = {
  id: string;
  date: string;
  mealItems: MealItem[];
  userId: number;
  glassOfWater: number;
  workOut: boolean;
  steps: number;
  cupOfCoffee: number;
  wakeUpTime: string;
  goSleepTime: string;
  mood: MoodType;
};

export const GET = async (request: Request) => {
  const url = request.url;
  const urlObject = new URL(url as string);
  const params = new URLSearchParams(urlObject.search);

  const userId = params.get("userId");
  const userDate = params.get("date");

  if (!userDate || !userId) {
    return new Response("There is no data!", {
      status: 500,
    });
  }
  const convertedDate = convertDateForDB(userDate);

  //get diary for a specific user and of a specific date, including all subitems(mealitems, food)
  const diary = await prisma.dayDiary.findFirst({
    where: {
      userId: parseInt(userId),
      date: convertedDate,
    },
    include: {
      mealItems: {
        include: {
          food: true,
        },
      },
    },
  });

  diary?.mealItems.sort((item1, item2) => {
    const time1 = item1.time ? new Date(item1.time).getTime() : null;
    const time2 = item2.time ? new Date(item2.time).getTime() : null;

    if (time1 !== null && time2 !== null) {
      return time1 - time2;
    }

    if (time1 === null || time2 === null) {
      return 1;
    }
    return 0;
  });

  return new Response(JSON.stringify(diary), {
    status: 200,
  });
};

export const POST = async (request: Request) => {
  const body: PostRequestBody = await request.json();
  const user = await prisma.user.findUnique({
    where: {
      id: body.userId,
    },
  });

  if (!user) {
    return new Response("User is not found", {
      status: 500,
    });
  }
  const convertedDate = convertDateForDB(body.date);

  await prisma.dayDiary.create({
    data: {
      userId: user.id,
      date: convertedDate,
      wakeUpTime: body.wakeUpTime,
      goSleepTime: body.goSleepTime,
      glassOfWater: body.glassOfWater,
      steps: body.steps,
      workOut: body.workOut,
      cupOfCoffee: body.cupOfCoffee,
      mood: body.mood,
      mealItems: {
        create: body.mealItems.map((item) => ({
          meal: item.meal,
          time: item.time,
          food: {
            create: item.food.map((foodItem) => ({
              description: foodItem.description,
              category: foodItem.category,
            })),
          },
        })),
      },
    },
  });

  return new Response(JSON.stringify("OK"), {
    status: 200,
  });
};

export const DELETE = async (request: Request) => {
  const url = request.url;
  const urlObject = new URL(url as string);
  const params = new URLSearchParams(urlObject.search);

  const id = params.get("id");

  await prisma.dayDiary.delete({
    where: {
      id: id as string,
    },
  });

  await prisma.mealItem.deleteMany({
    where: {
      dayDiaryId: null,
    },
  });

  await prisma.foodItem.deleteMany({
    where: {
      mealItemId: null,
    },
  });

  return new Response(JSON.stringify("OK"), {
    status: 200,
  });
};

export const PATCH = async (request: Request) => {
  const body: PostRequestBody = await request.json();
  const dayDiary = await prisma.dayDiary.findUnique({
    where: {
      id: body.id,
    },
  });

  if (!dayDiary) {
    return new Response("Diary is not found", {
      status: 500,
    });
  }
  const convertedDate = convertDateForDB(body.date);

  const diary = await prisma.dayDiary.update({
    where: {
      id: dayDiary.id,
    },
    data: {
      userId: body.userId,
      date: convertedDate,
      glassOfWater: body.glassOfWater,
      cupOfCoffee: body.cupOfCoffee,
      steps: body.steps,
      workOut: body.workOut,
      wakeUpTime: body.wakeUpTime,
      goSleepTime: body.goSleepTime,
      mood: body.mood,
      mealItems: {
        update: body.mealItems.map((item) => ({
          where: {
            id: item.id,
          },
          data: {
            meal: item.meal,
            time: item.time,
            food: {
              update: item.food.map((foodItem) => ({
                where: {
                  id: foodItem.id,
                },
                data: {
                  description: foodItem.description,
                  category: foodItem.category,
                },
              })),
            },
          },
        })),
      },
    },
  });

  return new Response(JSON.stringify(diary), {
    status: 200,
  });
};
