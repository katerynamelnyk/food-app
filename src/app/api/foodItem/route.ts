import prisma from "@/lib/prisma";

type PostRequestBody = {
  category: FoodCategory;
  description: string;
  mealItemId: string;
}[];

export const POST = async (request: Request) => {
  const body: PostRequestBody = await request.json();

  const mealItem = await prisma.mealItem.findFirst({
    where: {
      id: body[0].mealItemId as string,
    },
  });

  if (!mealItem) {
    return new Response("Meal item is not found", {
      status: 500,
    });
  }

  (async () => {
    for (const foodItem of body) {
      await prisma.foodItem.create({
        data: {
          mealItemId: foodItem.mealItemId,
          description: foodItem.description,
          category: foodItem.category,
        },
      });
    }
  })();

  return new Response(JSON.stringify("OK"), {
    status: 200,
  });
};
