import prisma from "@/lib/prisma";

type PostRequestBody = {
  meal: Meal;
  food: Food[];
  time: string;
}[];

export const POST = async (request: Request) => {
  const url = request.url;
  const urlObject = new URL(url as string);
  const params = new URLSearchParams(urlObject.search);

  const dayDiaryId = params.get("dayDiaryId");

  const body: PostRequestBody = await request.json();

  const dayDiary = await prisma.dayDiary.findUnique({
    where: {
      id: dayDiaryId as string,
    },
  });

  if (!dayDiary) {
    return new Response("Diary is not found", {
      status: 500,
    });
  }

  (async () => {
    for (const mealItem of body) {
      await prisma.mealItem.create({
        data: {
          dayDiaryId: dayDiaryId,
          meal: mealItem.meal,
          time: mealItem.time,
          food: {
            create: mealItem.food.map((foodItem) => ({
              description: foodItem.description,
              category: foodItem.category,
            })),
          },
        },
      });
    }
  })();

  return new Response(JSON.stringify("Ok"), {
    status: 200,
  });
};
