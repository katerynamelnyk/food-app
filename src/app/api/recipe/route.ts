import prisma from "@/lib/prisma";

export type PostRequestBody = {
  title: string;
  readyInMinutes: number;
  image: string;
  instructions: string;
  vegan: boolean;
  dairyFree: boolean;
  veryHealthy: boolean;
  ingredients: Ingredient[];
  userId: number;
};

export const GET = async (request: Request) => {
  const url = request.url;
  const urlObject = new URL(url as string);
  const params = new URLSearchParams(urlObject.search);
  const userId = params.get("userId");

  const recipies = await prisma.recipe.findMany({
    where: {
      OR: [
        {
          userId: parseInt(userId as string) || null,
        },
        {
          userId: null,
        },
      ],
    },
    include: {
      ingredients: true,
    },
  });

  return new Response(JSON.stringify(recipies), {
    status: 200,
  });
};

export const POST = async (request: Request) => {
  const body: PostRequestBody = await request.json();
  const user = await prisma.user.findUnique({
    where: {
      id: body.userId,
    },
  });

  await prisma.recipe.create({
    data: {
      title: body.title,
      instructions: body.instructions,
      image: body.image,
      readyInMinutes: body.readyInMinutes,
      vegan: body.vegan,
      dairyFree: body.dairyFree,
      veryHealthy: body.veryHealthy,
      ingredients: {
        create: body.ingredients.map((ingredient) => ({
          name: ingredient.name,
          amount: ingredient.amount,
          measure: ingredient.measure,
        })),
      },
      userId: user?.id,
    },
  });

  return new Response(JSON.stringify("OK!"), {
    status: 200,
  });
};
