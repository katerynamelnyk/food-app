import { Providers } from "./GlobalRedux/provider";
import Navigation from "@/components/Navigation/Navigation";
import "./global.css";
import styles from "./layout.module.scss";

export const metadata = {
  title: "Smart Eating",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={styles.main}>
        <Providers>
          <Navigation />
          {children}
        </Providers>
      </body>
    </html>
  );
}
