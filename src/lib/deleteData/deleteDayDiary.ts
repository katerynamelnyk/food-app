export const deleteDayDiary = async (id: string) => {
  const url = `/api/diary?id=${id}`;
  const response = await fetch(url, {
    method: "DELETE",
  });

  if (!response.ok) {
    throw new Error("Failed to send diary");
  }
  return response;
};
