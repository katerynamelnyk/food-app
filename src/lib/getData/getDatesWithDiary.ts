export const getDatesWithDiary = async (userId: number | null) => {
  const response = await fetch(`/api/dates?userId=${userId}`);

  if (!response.ok) {
    throw new Error("Failed to fetch days");
  }

  return response.json();
};
