export const getDiary = async (userId: number | null, date: string) => {
  const url = `api/diary?userId=${userId}&date=${date}`;

  const response = await fetch(url);
  if (!response.ok) {
    throw new Error("Failed to fetch data");
  }

  return response.json();
};
