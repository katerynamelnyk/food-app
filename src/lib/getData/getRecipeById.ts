export const getRecipeById = async (id: number) => {
  const url = `https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/${id}/information`;
  const options = {
    method: "GET",
    headers: {
      "X-RapidAPI-Key": "64870b4251msh63703dddf770610p1c236ajsnb07af9d98e02",
      "X-RapidAPI-Host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
    },
  };
  
  const response = await fetch(url, options);

  if (!response.ok) {
    throw new Error("Failed to fetch data");
  }

  return response.json();
};
