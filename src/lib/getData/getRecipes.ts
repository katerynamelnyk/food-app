export const getRecipes = async (inputValue: string) => {
  const query = inputValue.split(" ").join("%2C");

  const url = `https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/findByIngredients?ingredients=${query}&number=5&ignorePantry=true&ranking=1`;

  const options = {
    method: "GET",
    headers: {
      "X-RapidAPI-Key": "64870b4251msh63703dddf770610p1c236ajsnb07af9d98e02",
      "X-RapidAPI-Host": "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
    },
  };

  const response = await fetch(url, options);

  if (!response.ok) {
    throw new Error("Failed to fetch data");
  }

  return response.json();
};
