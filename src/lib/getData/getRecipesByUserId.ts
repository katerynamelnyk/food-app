export const getRecipesByUserId = async (userId: number) => {
    const url = `api/recipe?userId=${userId}`;
  
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error("Failed to fetch data");
    }
  
    return response.json();
}