export const getSelectedDiaries = async (
  userId: number | null,
  dateRange: String[]
) => {
  const url = `/api/diaries?userId=${userId}&startDate=${dateRange[0]}&endDate=${dateRange[1]}`;
  const response = await fetch(url);

  if (!response.ok) {
    throw new Error("Failed to fetch diaries");
  }

  return response.json();
};
