export * from "./deleteData";
export * from "./getData";
export * from "./sendData";
export * from "./updateData";
