export const sendDiary = async (dayDiary: DayDiary) => {
  const response = await fetch("/api/diary", {
    method: "POST",
    body: JSON.stringify(dayDiary),
  });

  if (!response.ok) {
    throw new Error("Failed to send diary");
  }
  return response;
};
