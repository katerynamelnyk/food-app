export const sendFoodItems = async (data: Food[]) => {
  const response = await fetch("/api/foodItem", {
    method: "POST",
    body: JSON.stringify(data),
  });

  if (!response.ok) {
    throw new Error("Failed to post food item");
  }

  return response;
};
