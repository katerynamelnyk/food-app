export const sendMealItems = async (data: MealItem[], dayDiaryId: string) => {
  const url = `/api/mealItems?dayDiaryId=${dayDiaryId}`;
  const response = await fetch(url, {
    method: "POST",
    body: JSON.stringify(data),
  });

  if (!response.ok) {
    throw new Error("Failed to send meal items");
  }

  return response.json();
};
