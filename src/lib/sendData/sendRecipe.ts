export const sendRecipe = async (recipe: RecipeLong) => {
  const response = await fetch("/api/recipe", {
    method: "POST",
    body: JSON.stringify(recipe),
  });

  if (!response.ok) {
    throw new Error("Failed to send a recipe");
  }

  return response;
};
