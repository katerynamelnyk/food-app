export type User = {
  name: string;
  email: string;
  password: string;
};

export const sendUser = async (user: User) => {
  const response = await fetch("/api/user", {
    method: "POST",
    body: JSON.stringify(user),
  });

  if (!response.ok) {
    throw new Error("Failed to create a user");
  }

  return response.json();
};
