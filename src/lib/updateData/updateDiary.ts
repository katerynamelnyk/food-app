export const updateDiary = async (dayDiary: DayDiary) => {
  const response = await fetch("/api/diary", {
    method: "PATCH",
    body: JSON.stringify(dayDiary),
  });

  if (!response.ok) {
    throw new Error("Failed to update diary");
  }

  return response.json();
};
