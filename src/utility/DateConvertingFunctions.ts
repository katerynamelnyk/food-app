export const convertDateToISOString = (date: Date | string): string => {
  const newDate = new Date(date);
  newDate.setHours(0, 0, 0, 0);
  return newDate.toISOString();
};

export const convertDateForDB = (date: Date | string): string => {
  const newDate = new Date(date);
  newDate.setHours(1, 0, 0, 0); //because of the time zone converting
  return newDate.toISOString();
};

export const convertDateForUI = (date: Date | string): string => {
  const newDate = new Date(date);
  const day = newDate.getDate();
  const monthName = newDate.toLocaleString("en-US", { month: "long" });
  return `${day} ${monthName}`;
};

export const convertDateToDay = (date: Date | string): string => {
  const newDate = new Date(date);
  return newDate.getDate().toString();
};

export const convertDateToMonth = (date: Date | string): string => {
  const newDate = new Date(date);
  return newDate.toLocaleString("en-US", { month: "long" });
};

export const convertDateToWeekDay = (date: string): string => {
  const day = new Date(date).getDay();
  const daysMap: { [key: number]: string } = {
    0: "Sunday",
    1: "Monday",
    2: "Tuesday",
    3: "Wednesday",
    4: "Thursday",
    5: "Friday",
    6: "Saturday",
  };
  return daysMap[day];
};
