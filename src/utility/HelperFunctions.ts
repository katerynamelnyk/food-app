export const sortDiary = (diary: DayDiary) => {
  diary?.mealItems.sort((item1, item2) => {
    const time1 = item1.time ? new Date(item1.time).getTime() : null;
    const time2 = item2.time ? new Date(item2.time).getTime() : null;

    if (time1 !== null && time2 !== null) {
      return time1 - time2;
    }

    if (time1 === null || time2 === null) {
      return 1;
    }
    return 0;
  });
};
