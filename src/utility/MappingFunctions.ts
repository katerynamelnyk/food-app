import {
  FoodCategory,
  Meal,
  MoodType,
} from "@prisma/client";

export const convertStringToFoodCategory = (category: string): FoodCategory => {
  const foodCategoryMap: { [key: string]: FoodCategory } = {
    vegetables: FoodCategory.vegetables,
    fruits: FoodCategory.fruits,
    cereals: FoodCategory.cereals,
    dairy: FoodCategory.dairy,
    sweets: FoodCategory.sweets,
    beverages: FoodCategory.beverages,
    snacks: FoodCategory.snacks,
    other: FoodCategory.other,
    meat: FoodCategory.meat,
    seafood: FoodCategory.seafood,
    bakery: FoodCategory.bakery,
    eggs: FoodCategory.eggs,
    makaroni: FoodCategory.makaroni,
    nuts: FoodCategory.nuts,
    desserts: FoodCategory.desserts,
  };
  return foodCategoryMap[category] || FoodCategory.other;
};

export const convertStringToMeal = (meal: string): Meal => {
  const mealMap: { [key: string]: Meal } = {
    breakfast: Meal.breakfast,
    lunch: Meal.lunch,
    dinner: Meal.dinner,
    snack: Meal.snack,
  };
  return mealMap[meal] || Meal.breakfast;
};

export const convertStringToMood = (mood: string): MoodType => {
  const moodMap: { [key: string]: MoodType } = {
    grateful: MoodType.grateful,
    energetic: MoodType.energetic,
    productive: MoodType.productive,
    peaceful: MoodType.peaceful,
    joyful: MoodType.joyful,
    excellent: MoodType.excellent,
    good: MoodType.good,
    neutral: MoodType.neutral,
    challenging: MoodType.challenging,
    bad: MoodType.bad,
    terrible: MoodType.terrible,
  };
  return moodMap[mood] || MoodType.neutral;
};
