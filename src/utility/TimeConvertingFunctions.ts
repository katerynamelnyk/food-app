export const getAverageTime = (timeData: string[]) => {
  const totalMinutes = timeData.reduce((accum, curVal) => {
    const [hour, minute] = curVal.split(":");
    return accum + parseInt(hour) * 60 + parseInt(minute);
  }, 0);

  const averageMinutes = totalMinutes / timeData.length;
  const hours = Math.round(averageMinutes / 60);
  const minutes = Math.round(averageMinutes % 60);
  const averageTime = `${String(hours).padStart(2, "0")}:${String(
    minutes
  ).padStart(2, "0")}`;
  return averageTime;
};

export const convertTimeForDB = (hours?: string, minute?: string): string => {
  const newDate = new Date();
  if (hours && minute) {
    newDate.setHours(Number(hours) + 1, Number(minute), 0, 0);
  }
  return newDate.toISOString();
};

export const convertTimeForUI = (date: Date | string): string => {
  const newDate = new Date(date);
  let hour = newDate.getHours() - 1; //because of the time zone converting
  if (hour === -1) {
    hour = 23;
  }
  const minute = newDate.getMinutes();
  return `${String(hour).padStart(2, "0")}:${String(minute).padStart(2, "0")}`;
};

export const convertHour = (date: Date | string): string => {
  const newDate = new Date(date);
  let hour = newDate.getHours() - 1;
  if (hour === -1) {
    hour = 23;
  }
  return hour.toString();
};

export const convertMinute = (date: Date | string): string => {
  const newDate = new Date(date);
  return newDate.getMinutes().toString();
};
