//RECIPE

type Ingredient = {
  id: number;
  name: string;
  amount: number;
  measure: string;
};

type RecipeShort = {
  id: number;
  image: string;
  title: string;
};

type RecipeLong = {
  id: number;
  userId?: number;
  title: string;
  readyInMinutes: number;
  image: string;
  instructions: string;
  vegan: boolean;
  dairyFree: boolean;
  veryHealthy: boolean;
  ingredients: Ingredient[];
};

//DIARY

type Meal = "breakfast" | "lunch" | "dinner" | "snack";

type FoodCategory =
  | "vegetables"
  | "fruits"
  | "cereals"
  | "dairy"
  | "sweets"
  | "beverages"
  | "snacks"
  | "other"
  | "meat"
  | "seafood"
  | "bakery"
  | "eggs"
  | "makaroni"
  | "desserts"
  | "nuts";

type MoodType =
  | "grateful"
  | "energetic"
  | "productive"
  | "peaceful"
  | "joyful"
  | "excellent"
  | "good"
  | "neutral"
  | "challenging"
  | "bad"
  | "terrible";

type Food = {
  id: string;
  mealItemId?: string;
  description: string;
  category: FoodCategory;
};

type MealItem = {
  id: string;
  dayDiaryId?: string;
  meal: Meal;
  food: Food[];
  time: string;
};

type DayDiary = {
  id: string;
  userId: number;
  date: string;
  mealItems: MealItem[];
  glassOfWater: number;
  workOut: boolean;
  steps: number;
  cupOfCoffee: number;
  wakeUpTime: string;
  goSleepTime: string | null;
  mood: MoodType;
};
